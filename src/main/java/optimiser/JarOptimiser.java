package optimiser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.util.jar.JarFile;
import java.io.File;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

import optimiser.parsers.RemoveFileParser;
import optimiser.parsers.SingleInvocParser;
import optimiser.structs.CallSites;
import optimiser.structs.CheckCastLines;
import optimiser.structs.ClassHierarchy;
import optimiser.structs.TargetMethods;
import optimiser.structs.WrapTargetMethods;
import optimiser.visitors.MethodCodeVisitor;
import optimiser.visitors.OptimiseVisitor;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ParameterNode;
import org.objectweb.asm.tree.TypeAnnotationNode;

public class JarOptimiser extends JarFile {


	// A sample main method to initialize and run the JarOptimiser
	public static void main(String[] arguments) throws IOException, FileNotFoundException, InterruptedException
	{
	
		String[] args = sortArgs(arguments);
		if (args == null)
			return;
		
		String in_jarName = args[0];		
		String out_jarName = args[1];
		String remove_filename = args[2];
		String staticInvoc_name = args[3];
		String inline_filename = args[4];
		int max_inlining_depth = Integer.parseInt(args[5]);
		String optimize_order = args[6];
		String unsound_filename = args[7];


		JarOptimiser opt = new JarOptimiser(in_jarName, out_jarName);
		opt.optimize(remove_filename, staticInvoc_name, inline_filename, unsound_filename, max_inlining_depth, optimize_order);
		opt.close();
	}


	// The name of the output jar
	private String outname;
	
	// The name of the input jar
	private String jarname;
	
	/************** Fields used in multithreading ************/

	// number of files in the input jar file
	int num_of_files;
	
	// all output files in array byte format.
	// Each thread writes one output file at a time at one array slot
	protected byte[][] outFiles;
	
	// all file names
	protected String[] outFilesNames;
	
	//The index that the next modified file will be written in the outFiles array
	protected int produce_next;
	
	// The index of the next array slot to consume by the main thread
	protected int consume_next;
	
	// The input files
	Enumeration<JarEntry> inFiles;
	
	// The output jar file
	JarOutputStream out;
	
	// the access modifiers of every field and method in every class. All members in this Set are implicitly public
	HashMap<String, Integer> publicMembers;
	
	/************** Statistics *******************/
	
	int invocations_inlined;
	int invocations_wrappedStatic;
	int invocations_directSpecial;
	int methods_removed;
	
	
	
	/************** Structures that store the optimization output files of doop ************/
	
	TargetMethods methsToRemove;
	WrapTargetMethods methsToStatic;
	CallSites singleInvocs;
	HashMap<String, HashMap<String,MethodNode>> methsToInline;
	CheckCastLines receiverAtTopOfStackLines_Global;
	CallSites singleInvocs_inline;
	ClassHierarchy hierarchy;
	HashMap<String, Boolean> externalOverriders;
	
	/***********************************************************************/
	

	public JarOptimiser(String jarname, String outname) throws IOException
	{
		super(jarname);
		this.jarname = jarname;
		this.outname = outname;
	}

	public JarOptimiser(File jarfile, String outname) throws IOException
	{
		super(jarfile);
		this.outname = outname;
	}
	

	public void optimize(String RemoveFile, String staticFile, String inlineFile, String unsoundFile, int max_inlining_depth, String order) throws FileNotFoundException, IOException, InterruptedException
	{
		long start = System.nanoTime();
		
		RemoveFileParser rfp = RemoveFileParser.create_parser(RemoveFile);
		rfp.parseAll();
		methsToRemove = rfp.getTargetMethods();
		rfp.close();
		
		SingleInvocParser sip = SingleInvocParser.create_parser(staticFile);
		sip.parseAll();
		methsToStatic = sip.getTargetMethods();
		singleInvocs = sip.getCallSites();
		sip.close();
		
		SingleInvocParser sip_inline = SingleInvocParser.create_parser(inlineFile);
		sip_inline.parseAll();
		WrapTargetMethods methSigsToInline = sip_inline.getTargetMethods();
		singleInvocs_inline = sip_inline.getCallSites();
		sip_inline.close();
		
		SingleInvocParser sip_unsound = SingleInvocParser.create_parser(unsoundFile);
		sip_unsound.parseAll();
		CallSites singleInvocs_unsound = sip_unsound.getCallSites();
		sip_unsound.close();
		
		//REMOVE UNSOUND CALLSITES
		singleInvocs_inline.removeUnsoundCallsites(singleInvocs_unsound);
		singleInvocs.removeUnsoundCallsites(singleInvocs_unsound);
		sip_unsound = null; singleInvocs_unsound = null;
		
		init_multithreading_vars();

		//printMethSigsToInline(methSigsToInline);
		
		// FIND THE THE CODE OF EACH METHOD THAT WILL BE INLINED IF INLINE IS ENABLED
		// <Classname, <method_sig, methodCode>>
		methsToInline = new HashMap<String, HashMap<String,MethodNode>>();
		receiverAtTopOfStackLines_Global = new CheckCastLines();
		if (true /*inlineEnabled(inlineFile, true) || devirtualEnabled(staticFile, true)*/) //TODO
		{
			Enumeration<JarEntry> entries = this.entries();
			while (entries.hasMoreElements())
			{
				JarEntry entry = entries.nextElement();
				
				
				String name = entry.getName();
				//System.out.println(name);
				if (name.endsWith(".class"))
				{					
					name =name.replaceAll("\\.class", "");	
					//System.out.println(name);
					byte[] bArray = toByteArray(entry);
					
					
					if (true /*methSigsToInline.containsKey(name) || singleInvocs.containsKey(name)*/)
					{	
						MethodCodeVisitor mcv = new MethodCodeVisitor(this, methSigsToInline.get(name));
						ClassReader cr = new ClassReader(bArray);
						cr.accept(mcv, ClassReader.EXPAND_FRAMES);
						
						methsToInline.put(name, mcv.getMethToInline());
						receiverAtTopOfStackLines_Global.put(name, mcv.getReceiverAtTopOfStackLines_ClassWide());
						
						methSigsToInline.remove(name);
					}
				}
				

			}
		}
		externalOverriders = hierarchy.transformToBool();
		//System.out.println("com/sun/tools/javac/code/Type$ClassType is subtype of com/sun/tools/javac/code/Type: " + hierarchy.isSubTypeOf("com/sun/tools/javac/code/Type$ClassType", "com/sun/tools/javac/code/Type"));
		//hierarchy.printBool();
		//hierarchy = null;
		
		
		//printMethsToInline();
		//printReceiverAtTopOfStackLines_Global();
		
		//TODO	ERROR WHEN MULTITHREADING
		int cores = 1;//Runtime.getRuntime().availableProcessors();
		System.out.println("cores: " + cores);
		for (int i=0; i<cores; i++)
		{	Thread t =	 new paralellOpt(staticFile, inlineFile, order, max_inlining_depth);
			t.start();
			t.join();
		}
		

		consume();

		out.close();
		
		long end = System.nanoTime();
		
		write_statistics((end-start));

	}
	
	private void optimiseAclass(String staticFile, String inlineFile, int max_inlining_depth, String order, Pair p)
	{
		JarEntry entry =  p.entry;
		byte[] bArray = toByteArray(entry);
		String og_name = entry.getName();
		
		if (og_name.endsWith(".class") && !og_name.equals("module-info.class")
				&& (inlineEnabled(inlineFile, true) || devirtualEnabled(staticFile, true))) //TODO module-info: WEIRD CORNER CASE. SUPER IS NOT OBJECT???
		{
			String name = og_name.replaceAll("\\.class", "");
			//System.out.println("filename: " + name);
			ClassReader cr = new ClassReader(bArray);
			if (cr.getAccess() != Opcodes.ACC_PUBLIC || methsToRemove.containsKey(name) || methsToStatic.containsKey(name) ||
				 singleInvocs.containsKey(name) || singleInvocs_inline.containsKey(name))
			{
				int parsingOptions = 0;
				int writerFlags = 0;
				boolean inlineEnabled = inlineEnabled(inlineFile, singleInvocs_inline.containsKey(name));
				boolean devirtualEnabled = devirtualEnabled(staticFile, singleInvocs.containsKey(name) || methsToStatic.containsKey(name));
				if (inlineEnabled || devirtualEnabled)
				{	parsingOptions = ClassReader.EXPAND_FRAMES;
					writerFlags = ClassWriter.COMPUTE_FRAMES;
				}
				
				ClassWriter cw; //ClassWriter.COMPUTE_FRAMES
				if (methsToRemove.containsKey(name))
					cw = new ClassWriter(writerFlags);
				else
					cw = new ClassWriter(cr, writerFlags);
				OptimiseVisitor rv = new OptimiseVisitor(this, cw, max_inlining_depth, order);

				cr.accept(rv, parsingOptions); //ClassReader.EXPAND_FRAMES
				bArray = cw.toByteArray();
	
				//data for this class is not needed anymore
				methsToRemove.remove(name); methsToStatic.remove(name);
				//if (!inlineEnabled)
					//singleInvocs.remove(name);
			}
		}
		
		writeProduced(og_name, bArray, p.indexToWrite);	
	}
	

	private void init_multithreading_vars() throws FileNotFoundException, IOException
	{
		num_of_files = size();
		outFiles = new byte[num_of_files][];
		outFilesNames = new String[num_of_files];
		consume_next = 0;
		produce_next = 0;	
		inFiles = this.entries();
		out = new JarOutputStream(new FileOutputStream(outname));	
		publicMembers = new HashMap<String, Integer>();
		hierarchy = new ClassHierarchy();
		fill_hierarchy();
		
	}
	
	private void fill_hierarchy()
	{
		Enumeration<JarEntry> entries = this.entries();
		while (entries.hasMoreElements())
		{
			JarEntry entry =  entries.nextElement();
			String og_name = entry.getName();
			
			if (og_name.endsWith(".class") && !og_name.equals("module-info.class"))
			{
				byte[] bArray = toByteArray(entry);
				String name = og_name.replaceAll("\\.class", "");
				ClassReader cr = new ClassReader(bArray);
				hierarchy.put(name, cr.getSuperName(), cr.getInterfaces());
			}
		}
		
	}

	protected class paralellOpt extends Thread {
		
		private Pair indexAndEntry;
		private String inlineFile;
		private String staticFile;
		String order;
		int max_inlining_depth;
		
		public paralellOpt(String staticFile, String inlineFile, String order, int max_inlining_depth)
		{
			indexAndEntry = new Pair();
			this.inlineFile = inlineFile;
			this.staticFile = staticFile;
			this.order = order;
			this.max_inlining_depth = max_inlining_depth;
		}
		
		@Override
		public void run() {
			
			while (getNextToProduce(this.indexAndEntry))
			{
				optimiseAclass(staticFile, inlineFile, max_inlining_depth, order, indexAndEntry);			
				
				
				
			}
			
		}
		
		
	}
	
	
	synchronized private boolean getNextToProduce(Pair p)
	{
		if (inFiles.hasMoreElements())
		{	p.indexToWrite = produce_next++;
			p.entry = inFiles.nextElement();
			return true;
		}
		return false;
	}
	
	synchronized private void writeProduced(String fileName, byte[] file, int writeIndex)
	{
		outFilesNames[writeIndex] = fileName;
		outFiles[writeIndex] = file;
		notify();
		
	}
	
	synchronized private void consume() throws InterruptedException, IOException
	{
		while (consume_next < num_of_files) {
			while (outFiles[consume_next] == null)
				wait();
			
			out.putNextEntry(new JarEntry(outFilesNames[consume_next]));
	    	out.write(outFiles[consume_next]);
	    	out.closeEntry();
	    	
	    	outFilesNames[consume_next] =null;
	    	outFiles[consume_next] = null;
	    	consume_next++;
		}
	}
	
	
	private boolean inlineEnabled(String inlineFile, boolean inline_methods_exist)
	{
		if (inlineFile == null)
			return false;
		return inline_methods_exist;
		
	}
	
	private boolean devirtualEnabled(String staticFile, boolean devirtual_methods_exist)
	{
		if (staticFile == null)
			return false;
		return devirtual_methods_exist;
	}


	public byte[] toByteArray(JarEntry entry)
	{	
		//TODO -1
		int size = (int)entry.getSize();
		byte[] bArray = new byte[size];
		
		try {
			InputStream is = this.getInputStream(entry);
	
			int bytes_read;
			for (int offset=0; (bytes_read = is.read(bArray, offset, size)) > 0; size-= bytes_read, offset+=bytes_read) {}
		}
		catch(IOException e)
		{	e.printStackTrace();	}
		
		return bArray;
		
	}
	
	
	/*** GETTERS ***/
	public ClassHierarchy getHierarchy() {
		return hierarchy;
	}

	public CallSites getSingleInvocs() {
		return singleInvocs;
	}

	public CallSites getSingleInvocs_inline() {
		return singleInvocs_inline;
	}

	public CheckCastLines getReceiverAtTopOfStackLines_Global() {
		return receiverAtTopOfStackLines_Global;
	}
		
	public HashMap<String, HashMap<String, MethodNode>> getMethsToInline() {
		return methsToInline;
	}
	
	public HashMap<String, Integer> getPublicMembers() {
		return publicMembers;
	}

	public HashMap<String, Boolean> getExternalOverriders() {
		return externalOverriders;
	}

	public TargetMethods getMethsToRemove() {
		return methsToRemove;
	}

	public WrapTargetMethods getMethsToStatic() {
		return methsToStatic;
	}

	/*** STATISTICS ***/
	synchronized public void incInvocations_inlined() {
		this.invocations_inlined++;
	}

	synchronized public void incInvocations_wrappedStatic() {
		this.invocations_wrappedStatic++;
	}

	synchronized public void incInvocations_directSpecial() {
		this.invocations_directSpecial++;
	}

	synchronized public void incMethods_removed() {
		this.methods_removed++;
	}
	


	class Pair {
		public int indexToWrite;
		public JarEntry entry;
		
		public void set(int i, JarEntry entry) {
			this.indexToWrite = i;
			this.entry = entry;		
		}
	}
	
	
	private void write_statistics(long time)
	{
		System.out.println(	"Invocations inlined: \t\t\t" + 					invocations_inlined +
							"\nTotal Invocations devirtualized: \t" +			(invocations_wrappedStatic+invocations_directSpecial) +
							"\nStaticly wrapped special Invocations: \t" + 		invocations_wrappedStatic +
							"\nDirectly called special Invocations: \t" +		invocations_directSpecial +
							"\nMethods removed: \t\t\t" + 						methods_removed +
							"\nTime taken: \t\t\t\t" +							(double)time / 1_000_000_000.0 + " seconds");
		
	}
	
	public static String[] sortArgs(String[] args)
	{
		String[] sorted_args = new String[8];
		boolean[] sorted_flags = new boolean[8];
		boolean error = false;
		sorted_args[5] = "0";
		sorted_args[6] = "devirtual";
		
		if (args.length % 2 != 0 || args.length == 0)
			error = true;
		else 
			for (int i=0; i<args.length; i+=2)
			{
				if (args[i].equals("-jar") && args[i+1].endsWith(".jar") && !sorted_flags[0])
				{	sorted_args[0] = args[i+1];
					sorted_flags[0] = true;
				}
				else if (args[i].equals("-o") && !sorted_flags[1])
				{	sorted_args[1] = args[i+1];
					sorted_flags[1] = true;
					if (!args[i+1].endsWith(".jar"))
						sorted_args[1] += ".jar";
				}
				else if (args[i].equals("-r") && args[i+1].endsWith(".csv") && !sorted_flags[2])
				{	sorted_args[2] = args[i+1];
					sorted_flags[2] = true;
				}
				else if (args[i].equals("-s") && args[i+1].endsWith(".csv") && !sorted_flags[3])
				{	sorted_args[3] = args[i+1];
					sorted_flags[3] = true;
				}
				else if (args[i].equals("-i") && args[i+1].endsWith(".csv") && !sorted_flags[4])
				{	sorted_args[4] = args[i+1];
					sorted_flags[4] = true;
				}
				else if (args[i].equals("-d") && !sorted_flags[5])
				{
					try {
						Integer.parseInt(args[i+1]);
					} catch (NumberFormatException e)
					{
						error = true;
						break;
					}
					sorted_args[5] = args[i+1];
					sorted_flags[5] = true;
				}
				else if (args[i].equals("-p") && (args[i+1].equals("devirtual") || args[i+1].equals("inline")) && !sorted_flags[6])
				{	sorted_args[6] = args[i+1];
					sorted_flags[6] = true;
				}
				else if (args[i].equals("-u") && args[i+1].endsWith(".csv") && !sorted_flags[7])
				{	sorted_args[7] = args[i+1];
					sorted_flags[7] = true;
				}
				else
				{	error=true;
					break;
				}

			}
		
		if (!error)
			for (int i=0; i<2; i++)
				if (!sorted_flags[i])
				{	error = true;
					break;
				}
			
		
		if (error)
		{	System.out.print("\nusage:" + "\t-jar <original.jar>\n"
						  + "\t-o <output.jar>\n"
						  + "\t-r <methodsToRemove.csv>\n"
						  + "\t-s <virtualToStatic.csv>\n"
						  + "\t-i <inline.csv>\n"
						  + "\t-u <unsoundCallsites.csv>\n"
						  + "\t-d <max_inlining_depth> (must be integer)\n"
						  + "\t-p <FLAG> (Sets the priority of optimizations for instructions that have multiple ones. FLAG must be 'devirtual' or 'inline')\n\n"
						  + "Input and Output jar names must be specified\n\n");
			return null;
		}

		return sorted_args;
	}

	

	
	
	
	
	
	public void printMethsToInline()
	{	
		Iterator<Map.Entry<String,HashMap<String,MethodNode>>> itt = methsToInline.entrySet().iterator();
		
		while (itt.hasNext())
		{	Map.Entry<String,HashMap<String,MethodNode>> entryy = itt.next();
			String nammee = entryy.getKey();
			
			if (nammee.equals("proguard/classfile/attribute/visitor/StackSizeComputer"))
			
			{
				Iterator<Map.Entry<String,MethodNode>> it = entryy.getValue().entrySet().iterator();
		
			System.out.println("----- " + nammee + " -----");

				while (it.hasNext())
				{
					Map.Entry<String,MethodNode> entry = it.next();
					String inclass = entry.getKey();
					MethodNode mn2 = entry.getValue();
					System.out.println("method: " +inclass);
				}
			}
		}
	}
	
	public void printMethSigsToInline(WrapTargetMethods methSigsToInline)
	{	
		Iterator<Map.Entry<String,HashMap<String,String>>> itt = methSigsToInline.entrySet().iterator();
		
		while (itt.hasNext())
		{	Map.Entry<String,HashMap<String,String>> entryy = itt.next();
			String nammee = entryy.getKey();
			
			if (nammee.equals("proguard/classfile/attribute/visitor/StackSizeComputer"))
			
			{
				Iterator<Map.Entry<String,String>> it = entryy.getValue().entrySet().iterator();
		
			System.out.println("----- " + nammee + " -----");

				while (it.hasNext())
				{
					Map.Entry<String,String> entry = it.next();
					String inclass = entry.getKey();
					String mn2 = entry.getValue();
					System.out.println("method: " +inclass + " .Sig: " + mn2);
				}
			}
		}
	}

	
	public void printReceiverAtTopOfStackLines_Global() {
		
		Iterator<Map.Entry<String, HashMap<String,HashMap<Integer, String>>>> itt = receiverAtTopOfStackLines_Global.entrySet().iterator();

		while (itt.hasNext())
		{	Map.Entry<String, HashMap<String,HashMap<Integer, String>>> entryy = itt.next();
			String nammee = entryy.getKey();
			Iterator<Map.Entry<String,HashMap<Integer, String>>> it = entryy.getValue().entrySet().iterator();
			
			//if (nammee.equals("proguard/classfile/attribute/visitor/StackSizeComputer"))
			//{

				while (it.hasNext())
				{
					Map.Entry<String,HashMap<Integer, String>> entry = it.next();
					String inclass = entry.getKey();
					Iterator<Map.Entry<Integer, String>> it3 = entry.getValue().entrySet().iterator();
					//System.out.println(nammee + "| method: " +inclass + " inline method -> ");
					while (it3.hasNext())
					{	Map.Entry<Integer, String> entry3 = it3.next();
						System.out.println(nammee + "| method: " +inclass + " inline method -> " + entry3.getKey() + " " + entry3.getValue() );
					}
				}
			//}
		}
		
		
		
	}
	
	
}






