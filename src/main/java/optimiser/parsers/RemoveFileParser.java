package optimiser.parsers;

import java.io.FileNotFoundException;

import java.util.regex.Matcher;

import optimiser.structs.TargetMethods;

import java.util.regex.MatchResult;

//<\w+(\.\w+)*(\$\w+)*:\s+\w+(\.\w+)*(\$\w+)*\s+(<init>|\w+)\((\w+(\.\w+)*(\$\w+)*(,\w+(\.\w+)*(\$\w+)*)*)?\)>
public class RemoveFileParser extends FileParser {

	TargetMethods map;

	//each line must start with < and end with >
	//have a : right after the class that the method belongs to
	//$ symbol is for inner classes
	//[] for matrixes. return types and args can be matrixes
	//<class: return_type meth_name(args)>
	private static final String REMOVE_FORMAT = "<\\w+(\\.\\w+)*(\\$\\w+)*:\\s+\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?\\s+(<init>|\\w+)\\((\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?(,\\w+(\\.\\w+)*(\\$\\w+)*(\\[\\])?)*)?\\)>";

	public TargetMethods getTargetMethods() {return map;}

	public RemoveFileParser(String filename) throws FileNotFoundException {
		super(filename, REMOVE_FORMAT);
		map = new TargetMethods();
	}
	
	public RemoveFileParser() {
		super();
		map = new TargetMethods();
	}
	
	public static RemoveFileParser create_parser(String filename) throws FileNotFoundException
	{
		new_descr_matcher();
		if (filename != null)
			return new RemoveFileParser(filename);
		return new RemoveFileParser();
	}

	@Override
	protected void parse(String line) {

		Matcher matcher = format_pattern.matcher(line);

		/*if (!matcher.matches())
			System.out.println(line + " - input line is incorrect");
		else
		{*/
			MatchResult result = scan(line, METHOD_FORMAT);
	     		
	     	map.set_method(result.group(1), result.group(2), result.group(3), result.group(4));
		//}
	}

/*
	public static void main(String[] Args) throws FileNotFoundException, IOException {

		RemoveFileParser rfp = new RemoveFileParser(Args[0]);
		Iterator<Map.Entry<String,HashSet<String>>> it = rfp.parseAll().entrySet().iterator();
		rfp.close();

		while (it.hasNext())
		{
			Map.Entry<String,HashSet<String>> entry = it.next();
			String inclass = entry.getKey();
			Iterator<String> it2 = entry.getValue().iterator();
			while (it2.hasNext())
				System.out.println(inclass + " " + it2.next());

			

		}
		

	}*/



} 
