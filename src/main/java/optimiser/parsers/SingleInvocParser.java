package optimiser.parsers;


import java.io.FileNotFoundException;

import java.util.regex.Matcher;

import optimiser.structs.CallSites;
import optimiser.structs.WrapTargetMethods;

import java.util.regex.MatchResult;
import java.util.HashMap;
import java.util.HashSet;


public class SingleInvocParser extends FileParser {

	CallSites map;
	WrapTargetMethods target_methods;

	private static final String SINGLE_INVOC_FORMAT = ".*"; //TODO


	public SingleInvocParser(String filename) throws FileNotFoundException {
		super(filename, SINGLE_INVOC_FORMAT);
		map = new CallSites();
		target_methods = new WrapTargetMethods();
	}
	
	public SingleInvocParser() {
		super();
		map = new CallSites();
		target_methods = new WrapTargetMethods();
	}
	
	public static SingleInvocParser create_parser(String filename) throws FileNotFoundException
	{
		new_descr_matcher();
		if (filename != null)
			return new SingleInvocParser(filename);
		return new SingleInvocParser();
	}

	@Override
	protected void parse(String line) {

		Matcher matcher = format_pattern.matcher(line);

		/*if (!matcher.matches())
			System.out.println(line + " - input line is incorrect");
		else
		{*/
		   	MatchResult result = scan(line, METHOD_FORMAT + "/(.*)/(\\d*)\\s+" + METHOD_FORMAT);
	     		
	     	HashMap<String, String[]> singleInvocs =  map.set_method(result.group(1), result.group(2), result.group(3), result.group(4)); 
	     	String staticInvok = result.group(5);
	     	String callIndex = result.group(6);
	     	String dynamicInvok = result.group(7);
	     	String dynamicRetType = result.group(8);
	     	String[] dyn_OwnerAndRetType = {dynamicInvok.replaceAll("\\.","/"), match_descriptor(dynamicRetType)};
	     	
	     	singleInvocs.put(FileParser.internal_name(staticInvok) + "\t" + callIndex, dyn_OwnerAndRetType);
	     	
	     	staticInvok = staticInvok.substring(0, staticInvok.lastIndexOf('.'));
	     	// owner class, return type, name, arguments
	     	target_methods.set_method(dynamicInvok, dynamicRetType, result.group(9), result.group(10));
	     	
	     	/*String staticInvokDescr = match_class_descriptor(staticInvok);
	     	if (!static_owners.contains(staticInvokDescr))
	     		static_owners.add(staticInvokDescr);*/
			
		//}
		
	}

	
	public CallSites getCallSites() {return map;}
	
	public WrapTargetMethods getTargetMethods() {return target_methods;}
	
/*
	public static void main(String[] Args) throws FileNotFoundException, IOException {

		SingleInvocParser sip = new SingleInvocParser(Args[0]);
		sip.parseAll();
		TargetMethods methsToStatic = sip.getTargetMethods();
		sip.close();
		
		Iterator<Map.Entry<String,ArrayList<String>>> it = methsToStatic.entrySet().iterator();


		while (it.hasNext())
		{
			Map.Entry<String,ArrayList<String>> entry = it.next();
			String inclass = entry.getKey();
			ArrayList<String> it2 = entry.getValue();
			for (int i=0; i< it2.size(); i++)
				System.out.println(inclass + " " + it2.get(i));

			

		}
		

	}*/



} 
