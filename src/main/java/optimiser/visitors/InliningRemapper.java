package optimiser.visitors;

import java.util.ArrayList;

import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
//import org.objectweb.asm.MethodWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;

import optimiser.JarOptimiser;
import optimiser.parsers.FileParser;

import org.objectweb.asm.commons.AnalyzerAdapter;

public class InliningRemapper extends LocalVariablesSorter {
	
	JarOptimiser jarOpt;

	private final LocalVariablesSorter lvs;
	private final TypeInferencer ti;
	
	private final Label end;
	private boolean isSynced;
	private boolean isStaticSynced;
	private int classObj ;
	
	
	private boolean last_is_return;
	private boolean last_is_xreturn;
	
	int starting_stack_size;
	String dynOwner;
	
	protected InliningRemapper(JarOptimiser jarOpt, LocalVariablesSorter lvs, TypeInferencer ti, int access, java.lang.String descriptor, String staticOwner, String dynOwner, String callSite) {
		super(Opcodes.ASM7, access | Opcodes.ACC_STATIC, "()V", lvs);
		
		this.jarOpt = jarOpt;
		
		this.lvs = lvs;
		this.ti = ti;
		this.end = new Label();
		
	    int offset = (access & Opcodes.ACC_STATIC)!=0 ? 0 : 1; 
	    Type[] args = Type.getArgumentTypes(descriptor);
	    int argsOffset = args.length;
		for (int i=0; i<args.length; i++)
			if (args[i].equals(Type.LONG_TYPE) || args[i].equals(Type.DOUBLE_TYPE))
				argsOffset++;
	    for (int i = args.length-1, j = argsOffset-1; i >= 0; i--, j--) {
	    	int opcode = args[i].getOpcode( Opcodes.ISTORE);
	    	if (opcode == Opcodes.LSTORE || opcode == Opcodes.DSTORE)
	    		j--;
	    	super.visitVarInsn(opcode, j + offset);
	    	//System.out.println(args[i] +"STORE " + (j + offset));
	    }
	    
	    isSynced = false;
	    isStaticSynced = false;
	    if(offset>0) {
	    	//TODO DONT CHECKAST IF THIS CALLSITE IS ALSO CANDIDATE FOR DEVIRTUALIZATION
	    	if (!jarOpt.getSingleInvocs().containsKey(callSite) && jarOpt.getHierarchy().isStrictSubTypeOf(dynOwner, staticOwner))
	    	{	super.visitTypeInsn(Opcodes.CHECKCAST, dynOwner);
				//System.out.println("CHECKCAST " + dynOwner);
	    	}
			if ((access & Opcodes.ACC_SYNCHRONIZED) != 0)
			{
				isSynced = true;
				super.visitInsn(Opcodes.DUP);
				super.visitInsn(Opcodes.MONITORENTER);
				//System.out.println("DUP");
				//System.out.println("MONITORENTER");
			}
	    	super.visitVarInsn(Opcodes.ASTORE, 0); 
	    	//System.out.println("ASTORE 0");
	    }
	    //TODO STATIC METHOD SYNCHRONIZE
	    else if ((access & Opcodes.ACC_SYNCHRONIZED) != 0)
	    {
	    	isStaticSynced = true;
	    	//Class clazz = Class.forName(FileParser.uninternal_name(dynOwner)); 
	    	super.visitLdcInsn(FileParser.uninternal_name(dynOwner));
	    	//System.out.println(FileParser.uninternal_name(dynOwner) + "<---------------------");
	    	super.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Class", "forName", "(Ljava/lang/String;)Ljava/lang/Class;", false);
	    	super.visitInsn(Opcodes.DUP);
	    	classObj = newLocal(Type.getObjectType(dynOwner));
	    	super.visitVarInsn(Opcodes.ASTORE, classObj);
	    	super.visitInsn(Opcodes.MONITORENTER);

	    	
	    }
	    starting_stack_size = ti.stack.size();
	    this.dynOwner = dynOwner;
	    
	    
	}

	//TODO THROW: you can't just jump! Exception must be propagated!!!
	@Override
	public void visitInsn(int opcode) { 
	    
	    //SAVE RETURN VAR IF IT EXISTS AND EMPTY CALLEE'S STACK (IF IT IS NOT EMPTY) WHEN EXITING THE INLINED CODE
		if (opcode >= Opcodes.IRETURN && opcode <= Opcodes.RETURN)
		{
			last_is_return = true;
			int current_stack_size = ti.stack.size();
			if (  current_stack_size > starting_stack_size + 1 )
			{	
				if (opcode == Opcodes.LRETURN)
					super.visitVarInsn(Opcodes.LSTORE, nextLocal); 		//TODO NEWLOCAL INSTEAD?
				else if (opcode == Opcodes.DRETURN)
					super.visitVarInsn(Opcodes.DSTORE, nextLocal);
				else if (opcode == Opcodes.IRETURN)
					super.visitVarInsn(Opcodes.ISTORE, nextLocal);
				else if (opcode == Opcodes.FRETURN)
					super.visitVarInsn(Opcodes.FSTORE, nextLocal);
				else if (opcode == Opcodes.ARETURN)
					super.visitVarInsn(Opcodes.ASTORE, nextLocal);
				
				current_stack_size = ti.stack.size();
				if (dynOwner.equals("proguard/ClassPathEntry"))
					System.out.println("yay");
				for (int i = ti.stack.size()-1; i>=starting_stack_size; i-- )
				{
					super.visitInsn(Opcodes.POP);
				}
				
				if (opcode == Opcodes.LRETURN)
					super.visitVarInsn(Opcodes.LLOAD, nextLocal);
				else if (opcode == Opcodes.DRETURN)
					super.visitVarInsn(Opcodes.DLOAD, nextLocal);
				else if (opcode == Opcodes.IRETURN)
					super.visitVarInsn(Opcodes.ILOAD, nextLocal);
				else if (opcode == Opcodes.FRETURN)
					super.visitVarInsn(Opcodes.FLOAD, nextLocal);
				else if (opcode == Opcodes.ARETURN)
					super.visitVarInsn(Opcodes.ALOAD, nextLocal);
			}
			
			
		}
		else {
			if (opcode == Opcodes.ATHROW && (isSynced || isStaticSynced))
			{
				if (isSynced)
					super.visitVarInsn(Opcodes.ALOAD, 0);
				else
					super.visitVarInsn(Opcodes.ALOAD, classObj);
				super.visitInsn(Opcodes.MONITOREXIT);
			}
	    	visitInsn();
	    	super.visitInsn(opcode); 
	    }
	} 
	
	@Override
	public void visitMaxs(int stack, int locals) { } 
	
	
	private void visitInsn() { 
		if (last_is_return)
		{	super.visitJumpInsn(Opcodes.GOTO, end);
			//System.out.println("GOTO " + end + " (switched from JUMP)");
			last_is_return = false;
		}
	}
	
	@Override
	protected int newLocalMapping(Type type) { 
		    return lvs.newLocal(type); 
	} 
	
	@Override
	public void visitEnd()
	{
		super.visitLabel(end);
		if (isSynced)
		{	super.visitVarInsn(Opcodes.ALOAD, 0);
			super.visitInsn(Opcodes.MONITOREXIT);
			//System.out.println("ALOAD 0");
			//System.out.println("MONITOREXIT");
		}
		else if (isStaticSynced)
		{
			super.visitVarInsn(Opcodes.ALOAD, classObj );
			super.visitInsn(Opcodes.MONITOREXIT);
		}
		super.visitEnd();
	}
	
	@Override
	  public void visitLabel(final Label label) { //TODO ADD VISIT FRAME AS WELL?
		  visitInsn();
		  super.visitLabel(label);
		  
	  }
		

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
		  visitInsn();
		  super.visitIntInsn(opcode, operand);
	    
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
		  visitInsn();
		  super.visitVarInsn(opcode, var);
	    
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
		  visitInsn();
		  super.visitTypeInsn(opcode, type);
	    
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
		  visitInsn();
		  super.visitFieldInsn(opcode, owner, name, descriptor);
	    
	  }

	  

	  @Override
	  public void visitMethodInsn(
	      final int opcode,
	      final String owner,
	      final String name,
	      final String descriptor,
	      final boolean isInterface) {
		  
		  visitInsn();
		  super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	    
	  }

	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
		  
		  visitInsn();
		  super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
	    
	  }

	  @Override
	  public void visitJumpInsn(final int opcode, final Label label) {
		  visitInsn();
		  super.visitJumpInsn(opcode, label);
	    
	  }
	  
	  @Override
	  public void visitLdcInsn(final Object value) {
		  visitInsn();
		  super.visitLdcInsn(value);
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
		  visitInsn();
		  super.visitIincInsn(var, increment);
	  }

	  @Override
	  public void visitTableSwitchInsn(
	      final int min, final int max, final Label dflt, final Label... labels) {
		  visitInsn();
		  super.visitTableSwitchInsn(min, max, dflt, labels);
	  }

	  @Override
	  public void visitLookupSwitchInsn(final Label dflt, final int[] keys, final Label[] labels) {
		  visitInsn();
		  super.visitLookupSwitchInsn(dflt, keys, labels);
	  }

	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
		  visitInsn();
		  super.visitMultiANewArrayInsn(descriptor, numDimensions);
	  }
	
	
}
