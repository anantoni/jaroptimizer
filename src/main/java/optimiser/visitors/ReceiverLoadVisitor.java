package optimiser.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.objectweb.asm.ConstantDynamic;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.tree.InsnList;

import optimiser.JarOptimiser;
import optimiser.structs.ClassHierarchy;
import optimiser.utils.ObjectRef;


//MAKE SURE NEXT VISITOR METHODS ARE CALLED IN CASE A METHOD NODE FOLLOWS. OTHERWISE INSTRUCTIONS WILL BE LOST DURING INLINE!
public class ReceiverLoadVisitor extends TypeInferencer implements CallSiteHelper {

	private int nextObjectRef;
	
	
	
	HashMap<ObjectRef, Integer> ReferenceAtTopLines;
	
	
	private HashMap<String, String[]> virtualToStatic;
	private HashMap<String, String[]> inlineCallSites;
	private HashMap<String, Integer> method_callsites; 
	private HashMap<Integer, String> checkcast_lines;
	private ClassHierarchy hierarchy;
	
	
	protected LineNumberVisitor lnv;
	
	
	String methName;
	String insideClass;
	String superName;
	String descriptor;

	JarOptimiser jarOpt;

	public ReceiverLoadVisitor(int api, JarOptimiser jarOpt, String owner, String superName, int access, String name, String descriptor,
			MethodVisitor methodVisitor, HashMap<Integer, String> checkcast_lines) {
		super(owner, access, name, descriptor, methodVisitor);

		this.jarOpt = jarOpt;
		
		ReferenceAtTopLines = new HashMap<ObjectRef, Integer>();
		
		nextObjectRef = 0;
		
		virtualToStatic = jarOpt.getSingleInvocs().get(owner, name + "\t" + descriptor);
		inlineCallSites = jarOpt.getSingleInvocs_inline().get(owner, name + "\t" + descriptor);
		method_callsites = new HashMap<String,Integer>();
		this.checkcast_lines = checkcast_lines;
		this.hierarchy = jarOpt.getHierarchy();
		
		

		
		
		methName = name;
		this.insideClass = owner;
		this.superName = superName;
		this.descriptor = descriptor;
	}
	
	
	
	private void visitInsn1(int opcode) {

		//System.out.println("\n"+opcode + " Opcode. name: "+ methName+descriptor);
		//printStack();
		  
		
		if (stack != null && !stack.isEmpty())
		{
			int i = stack.size()-1;
			
			Object type = stack.get(i);
		    if (!(type instanceof ObjectRef) || (type instanceof ObjectRef && opcode >= Opcodes.ILOAD && opcode <= Opcodes.ALOAD) )
		    {		    		
		    		stack.remove(i);
		    		ObjectRef objRef = new ObjectRef(nextObjectRef, null);
			    	stack.add(objRef);
			    	ReferenceAtTopLines.put(objRef, lnv.getLineNumber());
			    	nextObjectRef++;  	
		    	
			}
		    else if (type instanceof ObjectRef)
		    {
		    	ReferenceAtTopLines.put((ObjectRef)type, lnv.getLineNumber());
		    }
		}  
		    

	}
	
	
	@Override
	  public void visitMethodInsn(
	      final int opcode,
	      final String owner,
	      final String name,
	      final String descriptor,
	      final boolean isInterface) {
		
		if (name.equals("visitVariablesLongType"))
			System.out.println(name);
		
		if (opcode == Opcodes.INVOKEVIRTUAL || opcode == Opcodes.INVOKEINTERFACE)
		{				
			String callSite = getCallSite(method_callsites, owner, name);

			if (virtualToStatic.containsKey(callSite) || inlineCallSites.containsKey(callSite))
			{
				Type[] descrArray = Type.getArgumentTypes(descriptor);
				int receiverOffset = descrArray.length;
				for (int i=0; i<descrArray.length; i++)
					if (descrArray[i].equals(Type.LONG_TYPE) || descrArray[i].equals(Type.DOUBLE_TYPE))
						receiverOffset++;
				Object obj = stack.get(stack.size()-1 -receiverOffset);
				
				
				
				String dynamicReceiver = StaticOrInline(callSite);


				if (obj instanceof ObjectRef)
				{	
					ObjectRef objRef = (ObjectRef)obj;
					//System.out.println("Method: " + name + " staticObjectREf: " + objRef.getStaticRef() + " dynamic: " + dynamicReceiver);
					
					//if (!owner.equals(dynamicReceiver) && !superName.equals(dynamicReceiver))
					if (hierarchy.isStrictSubTypeOf(dynamicReceiver, owner) || !hierarchy.isSubTypeOf(owner, dynamicReceiver))
					{								
						int line = ReferenceAtTopLines.get(objRef);
						checkcast_lines.put(line, dynamicReceiver);	
					}/*
					else if (!hierarchy.isSubTypeOf(owner, dynamicReceiver))
					{
						virtualToStatic.remove(callSite);
						inlineCallSites.remove(callSite);
					}*/
				}
				//TODO safe mechanism. remove devirtualization of callsite if obj is not ObjectRef
				//else if (!owner.equals(dynamicReceiver))
					//virtualToStatic.remove(callSite);
					
				
				 
			}
			
		}
		
		
		super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
		visitInsn1(opcode);
	}
	
	private String StaticOrInline(String callSite) {
		
		String[] ownerANDretType;
		
		if ((ownerANDretType = virtualToStatic.get(callSite)) == null)
			ownerANDretType = inlineCallSites.get(callSite);
			
		return ownerANDretType[0];
		
	}
	
	



	@Override
	public void visitInsn(final int opcode) {


		super.visitInsn(opcode);
		
		if ((opcode < Opcodes.IRETURN || opcode > Opcodes.RETURN) && opcode != Opcodes.ATHROW)
			visitInsn1(opcode);
	    
	}

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
		  	super.visitIntInsn(opcode, operand);
		  	visitInsn1(opcode);
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
		  super.visitVarInsn(opcode, var);
		  visitInsn1(opcode);
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
		  super.visitTypeInsn(opcode, type);
		  visitInsn1(opcode);
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
	    super.visitFieldInsn(opcode, owner, name, descriptor);
	    visitInsn1(opcode);
	    
	  }

	  

	  

	  

	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
		  super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
		  visitInsn1(Opcodes.INVOKEDYNAMIC);
	  }



	  @Override
	  public void visitLdcInsn(final Object value) {
		  super.visitLdcInsn(value);
		  visitInsn1(Opcodes.LDC);
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
		  super.visitIincInsn(var, increment);
		  visitInsn1(Opcodes.IINC);
	  }


	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
		  super.visitMultiANewArrayInsn(descriptor, numDimensions);
		  visitInsn1(Opcodes.MULTIANEWARRAY);
	  }

	  @Override
	  public void visitLocalVariable(
	      final String name,
	      final String descriptor,
	      final String signature,
	      final Label start,
	      final Label end,
	      final int index) {
		  
		  super.visitLocalVariable(name, descriptor, signature, start, end, index);
		  
		  
	  }

	  @Override
	  public void visitMaxs(final int maxStack, final int maxLocals) {
		  super.visitMaxs(maxStack, maxLocals);
	  }

		public void setLnv(LineNumberVisitor lnv) {
			this.lnv = lnv;
		}
	  
	  public void printStack()
	  {
		  //System.out.println("\nSize: " + stack.size());
		  for (int i=0; i< stack.size(); i++)
			  System.out.println(stack.get(i));
	  }
}
