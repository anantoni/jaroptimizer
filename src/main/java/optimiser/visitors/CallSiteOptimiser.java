package optimiser.visitors;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

import optimiser.JarOptimiser;
import optimiser.structs.CallSites;
import optimiser.structs.CheckCastLines;
import optimiser.utils.ClassState;
import optimiser.utils.Const;
import optimiser.utils.SigCallsitePair;

import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

public class CallSiteOptimiser extends LocalVariablesSorter implements CallSiteHelper {

	JarOptimiser jarOpt;
	ClassState state;
	TypeInferencer ti;
	
	//protected CallSites allStaticCallSites;
	//protected CallSites allInlineCallSites;
	//protected CheckCastLines allCheckcastInsertLines;
	//protected HashMap<String, HashMap<String, MethodNode>> inlineMethods;
	protected HashSet<String> alreadyInlined;

	
	protected HashMap<String, Integer> method_callsites;
	
	protected HashMap<Integer, String> checkcastInsertLines;
	
	protected CallSites singleInvoc_inline;
	
	HashMap<String, String[]> toStaticCallSites;
	HashMap<String, String[]> toInlineCallSites;
	
	String adapterOrder;
	String insideClass;
	String superName;
	String meth_sig;
	
	protected boolean inlining;
	protected int max_inlining_depth;
	protected int inlining_depth;
	
	protected ArrayList<TryCatchBlockNode> blocks;
	

	protected int maxLocals;
		
	protected LineNumberVisitor lnv;
	protected Printer pr;
	
	
	public CallSiteOptimiser(JarOptimiser jarOpt, ClassState state, MethodVisitor methodVisitor, TypeInferencer ti, int access, java.lang.String descriptor,
			HashSet<String> alreadyInlined,
			String insideClass, String superName, String targetClass, String sig, String adapterOrder, 
			boolean inlining, ArrayList<TryCatchBlockNode> blocks, int max_inlining_depth, int inlining_depth, int currentLocal) {
		super(Opcodes.ASM7, access, descriptor, methodVisitor);
				
		this.jarOpt = jarOpt;
		this.state = state;
		this.ti = ti;
		
		//this.allInlineCallSites = allInlineCallSites;
		//this.allStaticCallSites = allStaticCallSites;
		//this.allCheckcastInsertLines = allCheckcastInsertLines;
		//this.inlineMethods = inlineMethods;
		if ( alreadyInlined == null)
			this.alreadyInlined = new HashSet<String>();
		else
			this.alreadyInlined = new HashSet<String>(alreadyInlined);
		this.alreadyInlined.add(targetClass + "\t" + sig);
		
		toInlineCallSites = jarOpt.getSingleInvocs_inline().get(targetClass, sig);
		toStaticCallSites = jarOpt.getSingleInvocs().get(targetClass, sig);
		checkcastInsertLines = jarOpt.getReceiverAtTopOfStackLines_Global().get(targetClass, sig);
				
		
		method_callsites = new HashMap<String,Integer>();
		this.insideClass = insideClass;
		this.superName = superName;
		this.meth_sig = sig;
		
		this.adapterOrder = adapterOrder;
		
		this.inlining = inlining;
		if ((this.max_inlining_depth = max_inlining_depth) <= 0)
			this.max_inlining_depth = 1;
		this.inlining_depth = inlining_depth + 1;
		if (blocks == null)
			this.blocks = new ArrayList<TryCatchBlockNode>();
		else
			this.blocks = blocks;
		
		this.maxLocals = 0;
		if (inlining)
			this.nextLocal = currentLocal;
			
		
	}
	

	
	@Override
	public void visitMethodInsn(int opcode, java.lang.String owner, java.lang.String name, java.lang.String descriptor, boolean isInterface)
	{	
		
		
		String callSite = getCallSite(method_callsites, owner, name);
		
		//System.out.println(callSite);
		//if (toStaticCallSites.containsKey(callSite))
			//System.out.println(callSite + " contained");
		//INLINE //TODO CHECK THESE INSIDE VALIDINLINECHECKER AS WELL
		if (toInlineCallSites.containsKey(callSite) &&
				inlining_depth < max_inlining_depth && 	(adapterOrder.equals(Const.INLINE) ||
														!toStaticCallSites.containsKey(callSite))
													&&	!name.equals("<init>")
													&&
		visitInlineInsn(opcode, owner, name, descriptor, isInterface, callSite))
		
		{
			
			jarOpt.incInvocations_inlined();
		
		}
		//DEVIRTUALIZE
		else if (toStaticCallSites.containsKey(callSite))// && (adapterOrder.equals(Const.DEVIRTUAL) ||
														//!toInlineCallSites.containsKey(callSite) ||
														//(toInlineCallSites.containsKey(callSite) && inlining_depth >= max_inlining_depth)))
		{
			
			visitStaticInsn(opcode, owner, name, descriptor, isInterface, callSite);
		}
		
		else
		{   
			mv.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
			//System.out.println("inside_class: " + insideClass +" depth: " + inlining_depth + " NO ACTION ---> " + " " + name + " " + descriptor);

	
		}
		
		visitInsn();
	}
	
	
	public void visitStaticInsn(int opcode, java.lang.String owner, java.lang.String name, java.lang.String descriptor, boolean isInterface, String callSite)
	{	//System.out.println("owner -- " + owner);
		
	
				String[] ownerANDretType = toStaticCallSites.get(callSite);
				String retType = ownerANDretType[1];
								
				String dynOwner = ownerANDretType[0];
				isInterface = false;
				
				
				// owner/dynOwner reflects the operand type. Operand type must be subclass of insideClass. Choose the subclass of the two
				if (jarOpt.getHierarchy().isSubTypeOf(dynOwner, owner))
					owner = dynOwner;
					
																												 
				if (insideClass.equals(dynOwner) || (superName.equals(dynOwner) && jarOpt.getHierarchy().isSubTypeOf(owner, insideClass)))
				{	opcode = Opcodes.INVOKESPECIAL;
					jarOpt.incInvocations_directSpecial();
				}
				else
				{	opcode = Opcodes.INVOKESTATIC;
					descriptor = "(L" + dynOwner + ";" + descriptor.substring(1).split("\\)", 2)[0] + ")" + retType;
					name += "$wrapper";
					jarOpt.incInvocations_wrappedStatic();
				}
				
				//System.out.println("depth: " + inlining_depth + " STATIC method -> " + " " + owner + " " + name + descriptor);

				 				
				mv.visitMethodInsn(opcode, dynOwner, name, descriptor, isInterface);
		
	}
	
	
	
	
	
	public boolean visitInlineInsn(int opcode, java.lang.String owner, java.lang.String name, java.lang.String descriptor, boolean isInterface, String callSite)
	{		
				
		String[] ownerANDretType = toInlineCallSites.get(callSite);
		String retType = ownerANDretType[1];
		String dynOwner = ownerANDretType[0];
			
		descriptor = descriptor.split("\\)", 2)[0] + ")" + retType;
		String dynamic_sig = name + "\t" + descriptor;
		
									  
		
		if (!isValidInline(opcode, dynOwner, name, descriptor, inlining_depth))
			return false;
		
		
		
		MethodNode mn = jarOpt.getMethsToInline().get(dynOwner).get(dynamic_sig);
				
		//System.out.println("depth: " + inlining_depth + " INLINE method -> " + dynOwner + " " + mn.name + " " + mn.desc);
		//Label end = new Label();
		mn.instructions.resetLabels();
		
		
		int starting_lineNumber = Type.getArgumentTypes(descriptor).length;
		if (!(opcode==Opcodes.INVOKESTATIC))
			starting_lineNumber++;
		
		//pr = new Textifier();
		//TraceMethodVisitor tmv = new TraceMethodVisitor(mv, pr);
		// TODO ACCESS FLAGS 0?
		state.setInlining(true);
		CallSiteOptimiser cso = new CallSiteOptimiser(jarOpt, state, mv, ti, mn.access, descriptor,
				//allStaticCallSites,
				//allInlineCallSites,
				//allCheckcastInsertLines,
				
				//inlineMethods,
				alreadyInlined,
				insideClass, superName, dynOwner, dynamic_sig, adapterOrder, 
				true, blocks, max_inlining_depth, inlining_depth, nextLocal);
		
		
		
		InliningRemapper ir = new InliningRemapper(jarOpt, cso, ti, mn.access, descriptor, owner, dynOwner, callSite);
		LineNumberVisitor lnv = new LineNumberVisitor(ir, 0);
		cso.setLnv(lnv); //DONT CHANGE THE ORDER cso.setLnv must be after creation of InliningREmapper
		//lnv.setMv(ir);
		//lnv.incrementLine();
		
		//import org.objectweb.asm.util.Printer;
		//import org.objectweb.asm.util.Textifier;

		
		//mn.accept(new InliningRemapper(cso, opcode==Opcodes.INVOKESTATIC ? Opcodes.ACC_STATIC : 0, descriptor, end, dynOwner)); 
		mn.accept(lnv); 

		
		if (cso.getMaxLocals() > maxLocals)
			maxLocals = cso.getMaxLocals();
		
		state.setInlining(inlining);
		//super.visitLabel(end);
		
		return true;
	}
	
	
	
	
	public void visitInsn() {
		  		
		if (lnv != null)
		{	int lineNumber = lnv.getLineNumber();
			if (checkcastInsertLines.containsKey(lineNumber))
			{	String dynamicReceiver = checkcastInsertLines.get(lineNumber);
				mv.visitTypeInsn(Opcodes.CHECKCAST, dynamicReceiver);
				//System.out.println(lineNumber + " <- checkcast to " + dynamicReceiver);
			}
		}
	}

	@Override
	  public void visitInsn(final int opcode) {
		
		  super.visitInsn(opcode);
		  visitInsn();
	    
	  }

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
		  
		  	super.visitIntInsn(opcode, operand);
		  	visitInsn();
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
		  
		  super.visitVarInsn(opcode, var);
		  visitInsn();
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
		  super.visitTypeInsn(opcode, type);
		  visitInsn();
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
		  
	    super.visitFieldInsn(opcode, owner, name, descriptor);
	    visitInsn();
	    
	  }


	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
		  super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
		  visitInsn();
	  }

	  @Override
	  public void visitJumpInsn(final int opcode, final Label label) {
		  
		  super.visitJumpInsn(opcode, label);
		  visitInsn();
	  }


	  @Override
	  public void visitLdcInsn(final Object value) {
		  super.visitLdcInsn(value);
		  visitInsn();
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
		  super.visitIincInsn(var, increment);
		  visitInsn();
	  }

	  @Override
	  public void visitTableSwitchInsn(
	      final int min, final int max, final Label dflt, final Label... labels) {
		  super.visitTableSwitchInsn(min, max, dflt, labels);
		  visitInsn();
	  }

	  @Override
	  public void visitLookupSwitchInsn(final Label dflt, final int[] keys, final Label[] labels) {
		  super.visitLookupSwitchInsn(dflt, keys, labels);
		  visitInsn();
	  }

	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
		  super.visitMultiANewArrayInsn(descriptor, numDimensions);
		  visitInsn();
	  }
	
	  
	  
	 /******* INLINE OVERRIDES ******/
	  
	  
	  @Override
	  public void visitEnd() {
		  
		    if (inlining && nextLocal > maxLocals)
		    	maxLocals = nextLocal;
		    super.visitEnd();
		  }
	  
	  @Override
		public void visitTryCatchBlock(Label start, Label end, Label handler, String type) { 
			    if(!inlining) { // TODO depth > 1
			      blocks.add(new TryCatchBlockNode(new LabelNode(start), new LabelNode(end),
			    		  new LabelNode(handler), type)); 
			    } 
			    else { 
			      super.visitTryCatchBlock(start, end, handler, type); 
			    } 
		} 
		  
		@Override
		public void visitMaxs(int stack, int locals) { 
			    Iterator<TryCatchBlockNode> it = blocks.iterator(); 
			    while(it.hasNext()) { 
			    	TryCatchBlockNode b = (TryCatchBlockNode) it.next(); 
			      super.visitTryCatchBlock(b.start.getLabel(), b.end.getLabel(), 
			          b.handler.getLabel(), b.type); 
			    } 
			    

			    //try {
			    mv.visitMaxs(stack, nextLocal > maxLocals ? nextLocal : maxLocals); 
			    /*}
			    catch (Exception e) {
			    	PrintWriter pw = new PrintWriter(System.err);
			    	((TraceMethodVisitor)mv).p.print(pw);
			    	pw.flush();
			    }*/
			    	

		 }
		
		/*********************************/
	
		public boolean isValidInline(int opcode, String targetClass, String name, String descr, int current_depth) {
			
			String sig = name + "\t" + descr;
			if (alreadyInlined.contains(targetClass + "\t" + sig))
				return false;
			
			Type[] descrArray = Type.getArgumentTypes(descr);
			int callerOffset = descrArray.length;
			for (int i=0; i<descrArray.length; i++)
				if (descrArray[i].equals(Type.LONG_TYPE) || descrArray[i].equals(Type.DOUBLE_TYPE))
					callerOffset++;
			if (opcode != Opcodes.INVOKESTATIC)
				callerOffset++;
			
			boolean emptyCallerStack = false;
			if (ti.stack.size() - callerOffset <= 0)
				emptyCallerStack = true;
			boolean isSameClass = insideClass.equals(targetClass);
			
			//CALLER AND CALLE BELONG IN THE SAME CLASS -> INVOKESPECIALS AREN'T DANGEROUS AND
			//CALLERSTACK IS EMPTY DURING THE INLINE    -> TRY-CATCH BLOCKS AREN'T DANGEROUS
			if (isSameClass && emptyCallerStack)
				return true;
			
			
			ValidInlineChecker vic = new ValidInlineChecker(jarOpt,
					targetClass, sig, inlining_depth+1, max_inlining_depth, isSameClass, !emptyCallerStack);

			MethodNode mn = jarOpt.getMethsToInline().get(targetClass).get(sig);
			mn.accept(vic);
			
			return vic.isInlinable();
				
		}

		
/*		public boolean isValidInline(HashMap<String, String[]> targetInlineCallSites, 
									MethodNodeSpecial mns, int current_depth) {
			
			
			ArrayList<SigCallsitePair> InvokeSepcialInsns = mns.getInvokeSepcialInsns();
			int size = InvokeSepcialInsns.size();
			
			// THERE ARE STILL INVOCESPECIALS, YET MAXIMUM DEPTH IS REACHED. ABORT INLINE
			if (size > 0 && current_depth >= max_inlining_depth )
				return false;
			
			
			//FOR EVERY INVOCESPECIAL OF THIS METHOD
			for (int i=0; i<size; i++)
			{	SigCallsitePair p = InvokeSepcialInsns.get(i);
				String sig = p.getSig();
				String callSite = p.getCallSite();
				
				// THIS INVOKESPECIAL IS NOT A CANDIDATE FOR INLINE (i.e Not specified by doop). ABORT INLINE
				if (!targetInlineCallSites.containsKey(callSite))
					return false;
				
				String[] ownerANDretType = targetInlineCallSites.get(callSite);
				String retType = ownerANDretType[1];
				String dynOwner = ownerANDretType[0];
				String dynamic_sig = sig.split("\\)", 2)[0] + ")" + retType;
				HashMap<String, String[]> callSites_next;
				
				callSites_next = allInlineCallSites.get(dynOwner, dynamic_sig);
				
				// RECURSIVELY CHECK INVOCESPECIALS INSNS OF THE TARGET METHOD OF THIS INVOCESPECIAL
				if (!isValidInline(callSites_next,
									(MethodNodeSpecial)inlineMethods.get(dynOwner).get(dynamic_sig),
									current_depth+1))
					return false;
			
			}	
			//SUCCESS
			//THIS METHOD EITHER DOES NOT CONTAIN INVOKESPECIALS OR ALL OF THE (including nested) INVOKESPECIALS ARE INLINABLE
			return true;
		}*/
			
		
		
		public void setLnv(LineNumberVisitor lnv) {
			this.lnv = lnv;
		}



		public int getMaxLocals()
		{
			return maxLocals;
		}
	
}




