//SAME AS METHODNODE
//ONLY DIFFERENCE: also delegate calls to next visitor
//EMIT STORED INSTRUCTIONS AT VISITEND


package optimiser.visitors;


import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.tree.MethodNode;

import optimiser.utils.ClassState;

public class MyMethodNode extends MethodNode {

	MethodVisitor mv;
	ClassState state;
	
	public MyMethodNode(
		      MethodVisitor mv, ClassState state,
		      final int access,
		      final String name,
		      final String descriptor,
		      final String signature,
		      final String[] exceptions) {
		
		super(Opcodes.ASM7, access, name, descriptor, signature, exceptions);
		this.mv = mv;
		this.state = state;
	}

	// -----------------------------------------------------------------------------------------------
	  // Implementation of the MethodVisitor abstract class
	  // -----------------------------------------------------------------------------------------------

	  @Override
	  public void visitParameter(final String name, final int access) {
	    super.visitParameter(name, access);
	    mv.visitParameter(name, access);
	  }

	  @Override
	  @SuppressWarnings("serial")
	  public AnnotationVisitor visitAnnotationDefault() {
	    return mv.visitAnnotationDefault();
	  }

	  @Override
	  public AnnotationVisitor visitAnnotation(final String descriptor, final boolean visible) {
	    return mv.visitAnnotation(descriptor, visible);
	  }

	  @Override
	  public AnnotationVisitor visitTypeAnnotation(
	      final int typeRef, final TypePath typePath, final String descriptor, final boolean visible) {
	    return mv.visitTypeAnnotation(typeRef, typePath, descriptor, visible);
	  }

	  @Override
	  public void visitAnnotableParameterCount(final int parameterCount, final boolean visible) {
	    super.visitAnnotableParameterCount(parameterCount, visible);
	    mv.visitAnnotableParameterCount(parameterCount, visible);
	  }

	  @Override
	  @SuppressWarnings("unchecked")
	  public AnnotationVisitor visitParameterAnnotation(
	      final int parameter, final String descriptor, final boolean visible) {
	    return mv.visitParameterAnnotation(parameter, descriptor, visible);
	  }

	  @Override
	  public void visitAttribute(final Attribute attribute) {
	    super.visitAttribute(attribute);
	    mv.visitAttribute(attribute);
	  }

	  @Override
	  public void visitFrame(
	      final int type,
	      final int numLocal,
	      final Object[] local,
	      final int numStack,
	      final Object[] stack) {
	    super.visitFrame(type, numLocal, local, numStack, stack);
	    mv.visitFrame(type, numLocal, local, numStack, stack);
	  }

	  @Override
	  public void visitInsn(final int opcode) {
	    super.visitInsn(opcode);
	    mv.visitInsn(opcode);
	  }

	  @Override
	  public void visitIntInsn(final int opcode, final int operand) {
	    super.visitIntInsn(opcode, operand);
	    mv.visitIntInsn(opcode, operand);
	  }

	  @Override
	  public void visitVarInsn(final int opcode, final int var) {
	    super.visitVarInsn(opcode, var);
	    mv.visitVarInsn(opcode, var);
	  }

	  @Override
	  public void visitTypeInsn(final int opcode, final String type) {
	    super.visitTypeInsn(opcode, type);
	    mv.visitTypeInsn(opcode, type);
	  }

	  @Override
	  public void visitFieldInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
	    super.visitFieldInsn(opcode, owner, name, descriptor);
	    mv.visitFieldInsn(opcode, owner, name, descriptor);
	  }

	  /**
	   * Deprecated.
	   *
	   * @deprecated use {@link #visitMethodInsn(int, String, String, String, boolean)} instead.
	   */
	  @Deprecated
	  @Override
	  public void visitMethodInsn(
	      final int opcode, final String owner, final String name, final String descriptor) {
	    if (api >= Opcodes.ASM5) {
	      super.visitMethodInsn(opcode, owner, name, descriptor);
	      return;
	    }
	    super.visitMethodInsn(opcode, owner, name, descriptor);
	    mv.visitMethodInsn(opcode, owner, name, descriptor);
	  }

	  @Override
	  public void visitMethodInsn(
	      final int opcode,
	      final String owner,
	      final String name,
	      final String descriptor,
	      final boolean isInterface) {
	    if (api < Opcodes.ASM5) {
	      super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	      return;
	    }
	    super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	    mv.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	  }

	  @Override
	  public void visitInvokeDynamicInsn(
	      final String name,
	      final String descriptor,
	      final Handle bootstrapMethodHandle,
	      final Object... bootstrapMethodArguments) {
	    super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
	    mv.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
	  }

	  @Override
	  public void visitJumpInsn(final int opcode, final Label label) {
	    super.visitJumpInsn(opcode, label);
	    mv.visitJumpInsn(opcode, label);
	  }

	  @Override
	  public void visitLabel(final Label label) {
	    super.visitLabel(label);
	    mv.visitLabel(label);
	  }

	  @Override
	  public void visitLdcInsn(final Object value) {
	    super.visitLdcInsn(value);
	    mv.visitLdcInsn(value);
	  }

	  @Override
	  public void visitIincInsn(final int var, final int increment) {
	    super.visitIincInsn(var, increment);
	    mv.visitIincInsn(var, increment);
	  }

	  @Override
	  public void visitTableSwitchInsn(
	      final int min, final int max, final Label dflt, final Label... labels) {
	    super.visitTableSwitchInsn(min, max, dflt, labels);
	    mv.visitTableSwitchInsn(min, max, dflt, labels);
	  }

	  @Override
	  public void visitLookupSwitchInsn(final Label dflt, final int[] keys, final Label[] labels) {
	    super.visitLookupSwitchInsn(dflt, keys, labels);
	    mv.visitLookupSwitchInsn(dflt, keys, labels);
	  }

	  @Override
	  public void visitMultiANewArrayInsn(final String descriptor, final int numDimensions) {
	    super.visitMultiANewArrayInsn(descriptor, numDimensions);
	    mv.visitMultiANewArrayInsn(descriptor, numDimensions);
	  }

	  @Override
	  public AnnotationVisitor visitInsnAnnotation(
	      final int typeRef, final TypePath typePath, final String descriptor, final boolean visible) {
	    // Find the last real instruction, i.e. the instruction targeted by this annotation.
	    return mv.visitInsnAnnotation(typeRef, typePath, descriptor, visible);
	  }

	  @Override
	  public void visitTryCatchBlock(
	      final Label start, final Label end, final Label handler, final String type) {
	    super.visitTryCatchBlock(start, end, handler, type);
	    mv.visitTryCatchBlock(start, end, handler, type);
	  }

	  @Override
	  public AnnotationVisitor visitTryCatchAnnotation(
	      final int typeRef, final TypePath typePath, final String descriptor, final boolean visible) {
	    return mv.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible);
	  }

	  @Override
	  public void visitLocalVariable(
	      final String name,
	      final String descriptor,
	      final String signature,
	      final Label start,
	      final Label end,
	      final int index) {
	    super.visitLocalVariable(name, descriptor, signature, start, end, index);
	    mv.visitLocalVariable(name, descriptor, signature, start, end, index);
	  }

	  @Override
	  public AnnotationVisitor visitLocalVariableAnnotation(
	      final int typeRef,
	      final TypePath typePath,
	      final Label[] start,
	      final Label[] end,
	      final int[] index,
	      final String descriptor,
	      final boolean visible) {
	    return mv.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible);
	  }

	  @Override
	  public void visitLineNumber(final int line, final Label start) {
	    super.visitLineNumber(line, start);
	    mv.visitLineNumber(line, start);
	  }

	  @Override
	  public void visitMaxs(final int maxStack, final int maxLocals) {
	    super.visitMaxs(maxStack, maxLocals);
	    mv.visitMaxs(maxStack, maxLocals);
	  }
/*
	  @Override
	  public void visitEnd() {
	    
		mv.visitEnd();
		  
		if (!state.isInlining())
			this.accept(mv);
			
	  }*/

	public MethodVisitor getMv() {
		return mv;
	}
	
	
	  
}
