package optimiser.visitors;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

import org.objectweb.asm.commons.SimpleRemapper;
import org.objectweb.asm.commons.MethodRemapper;

import optimiser.JarOptimiser;
import optimiser.parsers.SingleInvocParser;
import optimiser.structs.ClassHierarchy;
import optimiser.structs.WrapTargetMethods;

public class MethodCodeVisitor extends ClassVisitor {
	
	protected HashMap<String,String> methSigToInline;
	protected HashMap<String,MethodNode> methToInline;
	
	protected HashMap<String, HashMap<Integer, String>> receiverAtTopOfStackLines_ClassWide;
	

	
	String className;
	String superName;
	
	JarOptimiser jarOpt;
	
	public MethodCodeVisitor(JarOptimiser jarOpt, HashMap<String,String> methSigToInline) {
		super(Opcodes.ASM7, null);
		
		this.jarOpt = jarOpt;
		
		this.methSigToInline = methSigToInline;		
		this.methToInline = new HashMap<String,MethodNode>();
		
		this.receiverAtTopOfStackLines_ClassWide = new HashMap<String, HashMap<Integer, String>>();
	}
	
	@Override
	public void visit(final int version, final int access, final String name, final String signature, final String superName, final String[] interfaces) {
		    
				this.className = name;
				this.superName = superName;
				super.visit(version, access, name, signature, superName, interfaces);
		    
	}
	
	@Override
	public FieldVisitor visitField(int access, java.lang.String name, java.lang.String descriptor, java.lang.String signature, java.lang.Object value)
	{
		//if ((access & Opcodes.ACC_PUBLIC) != 0) 
		//make all fields public
			jarOpt.getPublicMembers().put(className + "\t" + name + "\t" + descriptor, Opcodes.ACC_PUBLIC);
		
		return null;
	}
	
	
	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		
		String sig = name + "\t" + desc;
		HashMap<String, HashMap<String, String[]>> singleInvocs = jarOpt.getSingleInvocs().get(className);
		HashMap<String, HashMap<String, String[]>> singleInvocs_inl = jarOpt.getSingleInvocs_inline().get(className);
		
		HashMap<String, Integer> publicMembers = jarOpt.getPublicMembers();
		if ((access & (Opcodes.ACC_PUBLIC | Opcodes.ACC_PROTECTED)) != 0)
			publicMembers.put(className + "\t" + sig, Opcodes.ACC_PUBLIC);
		else
			//WE DON'T CARE ABOUT DISTINCTION BETWEEN PRIVATE OR PACKAGE PRIVATE
			publicMembers.put(className + "\t" + sig, Opcodes.ACC_PRIVATE);
		
		int startingLine = 0;
		ReceiverLoadVisitor rlv = null;
		LineNumberVisitor lnv =null;
		
		if (methSigToInline != null && methSigToInline.containsKey(sig) &&
				((singleInvocs != null && singleInvocs.containsKey(sig)) || (singleInvocs_inl != null && singleInvocs_inl.containsKey(sig)) )) {
			
			MethodNode mn = new MethodNode(Opcodes.ASM7, access, name, desc, signature, exceptions);
			methToInline.put(sig, mn);
			
			HashMap<Integer, String> checkcast_lines = new HashMap<Integer, String>();
			receiverAtTopOfStackLines_ClassWide.put(sig, checkcast_lines);		
			rlv = new ReceiverLoadVisitor(Opcodes.ASM7, jarOpt, className, superName, access, name, desc, mn, checkcast_lines);
		}
		else if (methSigToInline != null && methSigToInline.containsKey(sig)) {
			
			MethodNode mn = new MethodNode(Opcodes.ASM7, access, name, desc, signature, exceptions);
			methToInline.put(sig, mn);
			return mn;
		}
		else if ((singleInvocs != null && singleInvocs.containsKey(sig)) || (singleInvocs_inl != null && singleInvocs_inl.containsKey(sig)))
		{
			HashMap<Integer, String> checkcast_lines = new HashMap<Integer, String>();
			receiverAtTopOfStackLines_ClassWide.put(sig, checkcast_lines);
			rlv = new ReceiverLoadVisitor(Opcodes.ASM7, jarOpt, className, superName, access, name, desc, null, checkcast_lines);
		}
		
		if (rlv != null)
		{	lnv = new LineNumberVisitor(rlv, startingLine);
			rlv.setLnv(lnv);
		}
				
		return lnv;
	}


	public HashMap<String, MethodNode> getMethToInline() {
		return methToInline;
	}

	public HashMap<String, HashMap<Integer, String>> getReceiverAtTopOfStackLines_ClassWide() {
		return receiverAtTopOfStackLines_ClassWide;
	}
	
	

	
	

	
	/*
	public static void main(String[] Args) throws FileNotFoundException, IOException {

		SingleInvocParser sip = new SingleInvocParser(Args[0]);
		sip.parseAll();
		WrapTargetMethods methsToIn = sip.getTargetMethods();
		sip.close();
		
		Iterator<Map.Entry<String, HashMap<String,HashSet<String>>>> it = methsToIn.entrySet().iterator();
		
		MethodCodeVisitor mcv = new MethodCodeVisitor(methsToIn);
		


		while (it.hasNext())
		{
			Map.Entry<HashMap<String,HashSet<String>>> entry = it.next();
			String inclass = entry.getKey();
			ArrayList<String> it2 = entry.getValue();
			for (int i=0; i< it2.size(); i++)
				System.out.println(inclass + " " + it2.get(i));

			

		}
		

	} */
}

