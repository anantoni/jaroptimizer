package optimiser.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.jar.JarEntry;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

import optimiser.utils.SigCallsitePair;

public class MethodNodeSpecial extends MethodNode {

	ArrayList<SigCallsitePair> invokeSepcialInsns;
	
	private HashMap<String, Integer> special_callsites;
	
	
	
	
	
	
	
	public MethodNodeSpecial(int api, int access, String name, String descriptor, String signature,
			String[] exceptions) {
		super(api, access, name, descriptor, signature, exceptions);
		
		invokeSepcialInsns = new ArrayList<SigCallsitePair>();
		
		special_callsites = new HashMap<String, Integer>();
	}
	
	
	@Override
	public void visitMethodInsn(int opcode,
            java.lang.String owner,
            java.lang.String name,
            java.lang.String descriptor,
            boolean isInterface) {
		
		if (opcode == Opcodes.INVOKESPECIAL && !name.equals("<clinit>") && !name.equals("<init>"))
		{
			
		
			String method_call = owner + "." + name;
			if (!special_callsites.containsKey(method_call))
				special_callsites.put(method_call, 0);
			
			int call_counter = special_callsites.get(method_call);
			
			String callSite = method_call + "\t" + call_counter;
			special_callsites.put(method_call, ++call_counter);
			
			SigCallsitePair pair = new SigCallsitePair(name + "\t" + descriptor, callSite);
			
			invokeSepcialInsns.add(pair);
		
		
		}
		
		super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	}
	
	@Override
	public void visitEnd()
	{
		//delete this. Not needed
		special_callsites = null;
		
		super.visitEnd();
	}


	public ArrayList<SigCallsitePair> getInvokeSepcialInsns() {
		return invokeSepcialInsns;
	}


	

	

}
