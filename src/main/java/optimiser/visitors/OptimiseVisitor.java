package optimiser.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import optimiser.JarOptimiser;
import optimiser.structs.CallSites;
import optimiser.structs.CheckCastLines;
import optimiser.structs.WrapTargetMethods;
import optimiser.utils.ClassState;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import org.objectweb.asm.commons.AnalyzerAdapter;

public class OptimiseVisitor extends ClassVisitor {

	JarOptimiser jarOpt;
	
	protected String className;
	protected String superName;
	
	// Upper bound of times a method within a method e.t.c can be inlined. i.e foo inlines bar which inlines boo is a depth of 2.
	// default value is 1
	int max_inlining_depth;
	
	String adapterOrder;
	
	ArrayList<MyMethodNode> myNodes;

	public OptimiseVisitor(JarOptimiser jarOpt,
			ClassVisitor cv,
			int max_inlining_depth,
			String adapterOrder) {
		super(Opcodes.ASM7, cv);
		this.jarOpt = jarOpt;


		this.max_inlining_depth = max_inlining_depth;
		this.adapterOrder = adapterOrder;
		

		this.myNodes = new ArrayList<MyMethodNode>();
	}
	
	@Override
	public void visit(final int version, final int access, final String name, final String signature, final String superName, final String[] interfaces) {
		    
				this.className = name;
				this.superName = superName;
				int public_access = access;
				if (!name.equals("module-info"))
					public_access = access | Opcodes.ACC_PUBLIC;
				super.visit(version, public_access, name, signature, superName, interfaces);
		    
		  }
	
	//TODO SAME SEMANTICS?
	@Override
	public FieldVisitor visitField(int access, java.lang.String name, java.lang.String descriptor, java.lang.String signature, java.lang.Object value)
 	{
		return super.visitField((access & ~Opcodes.ACC_PRIVATE & ~Opcodes.ACC_PROTECTED) | Opcodes.ACC_PUBLIC, name, descriptor, signature, value);
 	}


	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		
		String sig = name + "\t" + desc;
		HashSet<String> rem_methods = jarOpt.getMethsToRemove().get(className);
		if (rem_methods != null && rem_methods.contains(sig)) {
			// do not delegate to next visitor -> this removes the method
			jarOpt.incMethods_removed();
			return null;
		}
		
		//CREATE WRAPPERS FOR METHOD
		/*HashMap<String, String> stc_methods = jarOpt.getMethsToStatic().get(className);
		if (stc_methods != null && stc_methods.containsKey(sig))
		{
			String dyn_owner = stc_methods.get(sig);
			//while (static_owners.hasNext())
			
				createMethod(name, desc, dyn_owner, exceptions);
			
		}*/
		
		if ((access & Opcodes.ACC_PROTECTED) != 0)
			access = (access & ~Opcodes.ACC_PROTECTED) | Opcodes.ACC_PUBLIC;
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
		
		int inlining_depth = -1;
		int starting_line = 0;
		//System.out.println("---" + name+"---");
		mv = chooseVisitor(jarOpt, mv, access, name, desc, signature, exceptions,
//				singleInvocs,
//				singleInvoc_inline,
//				receiverAtTopOfStackLines_ClassWide,

//				methodsToInline,
				null, //already Inlined Methods
				className, superName, className, sig, adapterOrder, starting_line,
				false, null, max_inlining_depth, inlining_depth, 0);
		return mv;
		
	
	}
	
	
	public MethodVisitor chooseVisitor(JarOptimiser jarOpt, MethodVisitor methodVisitor, int access, String name, 
			String descriptor, String signature, String[] exceptions,
			HashSet<String> alreadyInlined,
			String insideClass, String superName, String targetClass, String sig, String adapterOrder, int startingLine,
			boolean inlining, ArrayList<TryCatchBlockNode> blocks, int max_inlining_depth, int inlining_depth, int currentLocal) {
		
		CallSites allInlineCallSites = jarOpt.getSingleInvocs_inline();
		HashMap<String, String> stc_methods = jarOpt.getMethsToStatic().get(className);
		ClassState state = new ClassState(false);
		
		//WRAPPERS
		if (stc_methods != null && stc_methods.containsKey(sig))
		{
			String dyn_owner = stc_methods.get(sig);
			String new_descr = "(" + dyn_owner + descriptor.substring(1);
			
			MyMethodNode myNode = new MyMethodNode(methodVisitor, state, Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, name+"$wrapper",
					new_descr, null, exceptions);
			myNodes.add(myNode);
			methodVisitor = myNode;
		}
		
		
		if (jarOpt.getSingleInvocs().containsKey(targetClass, sig) || allInlineCallSites.containsKey(targetClass, sig))
		{	TypeInferencer ti = null;
			if (allInlineCallSites.containsKey(targetClass, sig))
				methodVisitor = ti = new TypeInferencer(insideClass, access, name, descriptor, methodVisitor);
			Printer pr = new Textifier();
			methodVisitor = new TraceMethodVisitor(methodVisitor, pr);
			CallSiteOptimiser cso = new CallSiteOptimiser(jarOpt, state, methodVisitor, ti, access, descriptor,
					alreadyInlined,
					insideClass, superName, targetClass, sig, adapterOrder,
					inlining, blocks, max_inlining_depth, inlining_depth, currentLocal);
		
			methodVisitor = new LineNumberVisitor(cso, startingLine);
			
			cso.setLnv((LineNumberVisitor)methodVisitor);
			
		}
		
		
		return methodVisitor;
	}
	
	@Override
	public void visitEnd()
	{
		for (MyMethodNode myNode: myNodes)
		{
			List<String> ex = myNode.exceptions;
			myNode.accept(cv.visitMethod(myNode.access, myNode.name, myNode.desc, myNode.signature, ex.toArray(new String[ex.size()])));
		}
		
		super.visitEnd();
	}

	private void createMethod(String methName, String descr, String staticOwner, String[] exceptions)
	{
		String stc_descr = "(" + staticOwner + descr.substring(1);
		
		// ALWAYS NULL SIGNATURE?
		MethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, methName+"$wrapper", stc_descr, null, exceptions);
		mv.visitCode();
		
		Type[] args = Type.getArgumentTypes(stc_descr);
		int var_number=0;

		for (int i=0; i<args.length; i++)
		{
			var_number += argLoad(mv, args[i], var_number);
			//if (i==0)
				//mv.visitTypeInsn(Opcodes.CHECKCAST, className);
		}
		mv.visitMethodInsn(Opcodes.INVOKESPECIAL, className, methName, descr, false);
		
		int retLength = 1;
		Type ret_type = Type.getReturnType(stc_descr);
		int ret_opcode;
		if (ret_type.equals(Type.LONG_TYPE))
		{	ret_opcode =Opcodes.LRETURN;
			retLength++;
		}
		else if (ret_type.equals(Type.FLOAT_TYPE))
			ret_opcode = Opcodes.FRETURN;
		else if (ret_type.equals(Type.DOUBLE_TYPE))
		{	ret_opcode = Opcodes.DRETURN;
			retLength++;
		}
		else if (ret_type.equals(Type.INT_TYPE) || ret_type.equals(Type.BOOLEAN_TYPE) || ret_type.equals(Type.BYTE_TYPE) || ret_type.equals(Type.CHAR_TYPE) || ret_type.equals(Type.SHORT_TYPE))
			ret_opcode = Opcodes.IRETURN;
		else if (ret_type.equals(Type.VOID_TYPE))
		{	ret_opcode = Opcodes.RETURN;
			retLength--;
		}
		else
			ret_opcode = Opcodes.ARETURN;
		mv.visitInsn(ret_opcode);
		
		
		mv.visitMaxs(var_number > retLength ? var_number : retLength, var_number );
		mv.visitEnd();
	}
	
	
	private int argLoad(MethodVisitor mv, Type arg, int var_number)
	{
			int xLoad;
			int var_size=1;
			if (arg.equals(Type.LONG_TYPE))
			{	xLoad = Opcodes.LLOAD;
				var_size++;
			}
			else if (arg.equals(Type.FLOAT_TYPE))
				xLoad = Opcodes.FLOAD;
			else if (arg.equals(Type.DOUBLE_TYPE))
			{	xLoad = Opcodes.DLOAD;
				var_size++;
			}
			else if (arg.equals(Type.INT_TYPE) || arg.equals(Type.BOOLEAN_TYPE) || arg.equals(Type.BYTE_TYPE) || arg.equals(Type.CHAR_TYPE) || arg.equals(Type.SHORT_TYPE))
				xLoad = Opcodes.ILOAD;
			else
			{	xLoad = Opcodes.ALOAD;
			}
			
			mv.visitVarInsn(xLoad, var_number);
			
	
			return var_size;
	}
	
	

}
