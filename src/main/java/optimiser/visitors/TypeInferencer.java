package optimiser.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.objectweb.asm.ConstantDynamic;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.tree.InsnList;

import optimiser.utils.ObjectRef;

// USAGE:
// what AnalyzerAdapter does(inference rules to compute frames) but makes sure the operand stack won't be null after GOTO, XRETURNS or ATHROW

/* This MethodVisitor works on the assumption that by using inference rules 
 * the length and general types of the operand stack can be computed with a single pass.
 * while loops are the ones that need multiple passes but we don't care about which specific subclass exists in each operand stack slot
 * in our use cases:
 * 1. Find in which instruction a receiver was loaded in ReceiverLoadVisitor
 * 2. Find the size of the operand stack in CallSiteOptimiser
 */
//hence this approach works with classes regardless of whether they have stack map frames or not

//BUG: DOESN'T KNOW THE LOCAL VARIABLES INSIDE A CATCH BLOCK. USE WITH CARE
// need the exception table to solve this but it is visited after the code and we don't store it somewhere beforehand

public class TypeInferencer extends AnalyzerAdapter {


	private HashMap<Label, List<Object>> stacks;
	private HashMap<Label, List<Object>> localsMap;
	


	public TypeInferencer(String owner, int access, String name, String descriptor, MethodVisitor methodVisitor) {
		super(Opcodes.ASM7, owner, access, name, descriptor, methodVisitor);

		stacks = new HashMap<Label, List<Object>>();
		localsMap = new HashMap<Label, List<Object>>();
		
	}
	


	//We don't need to use any frames
	@Override
	  public void visitFrame(
	      final int type,
	      final int numLocal,
	      final Object[] local,
	      final int numStack,
	      final Object[] stack) {
	    
	  }



	@Override
	public void visitInsn(final int opcode) {

		super.visitInsn(opcode);
		  
		//SEE GOTO INSTRUCTION
		if ((opcode >= Opcodes.IRETURN && opcode <= Opcodes.RETURN) || opcode == Opcodes.ATHROW) {
			stack = new ArrayList<Object>();
			locals = new ArrayList<Object>();
			stack.add("java/lang/Exception");
		}
		//else
			//super.visitInsn(opcode);
		
	    
	}

	  @Override
	  public void visitJumpInsn(final int opcode, final Label label) {
			//System.out.println(opcode + " Opcode. Stack Size: " + stack.size() + " before exec");

		  
			//goto super makes stack null. not good since we might use it after
			if (opcode != Opcodes.GOTO)
				super.visitJumpInsn(opcode, label);
			else if (mv != null)
				mv.visitJumpInsn(opcode, label);
			
		  if (!stacks.containsKey(label))
		  {	  
			  stacks.put(label, new ArrayList<Object>(stack));
			  localsMap.put(label, new ArrayList<Object>(locals));
			  
		  }
		  	
		  //TODO IF THE LABELS FOR THE NEXT INSTRUCTION DON'T SPECIFY A STACK
		  //IT'S A CATCH BLOCK (AS FAR AS I KNOW)
		  //OTHERWISE THE LABELS FOLLOWING THIS GOTO WILL OVERRIDE THIS SAFE MECHANISM
		  //SAME FOR ATHROW AND RETURN INSTRUCTIONS
		  if (opcode == Opcodes.GOTO) {
			  stack = new ArrayList<Object>();
			  locals = new ArrayList<Object>();
			  stack.add("java/lang/Exception");
		  }
	  }

	  // stacks that were stored in jump instructions are retrieved here (i.e the target of those jump instructions)
	  @Override
	  public void visitLabel(final Label label) {
		  	  super.visitLabel(label);
		  	    
				 //System.out.println("Label " +lnv.getLineNumber() + " " + owner + " " + methName + descriptor);
				  
				  if (stacks.containsKey(label))
				  {		
				  		stack = new ArrayList<Object>(stacks.get(label));
				  		locals = new ArrayList<Object>(localsMap.get(label));
				  		//System.out.println("already visited (forward jump) ");
				  		
				  }
		  	  
		  
	  }

	  
	  @Override
	  public void visitTableSwitchInsn(
	      final int min, final int max, final Label dflt, final Label... labels) {
			//System.out.println("Tableswitch Opcode. Stack Size: " + stack.size() + " before exec");
			if (mv!=null)
				mv.visitTableSwitchInsn(min, max, dflt, labels);
		  stack.remove(stack.size() - 1);
		  for (int i=0; i< labels.length; i++)
		  {		stacks.put(labels[i], new ArrayList<Object>(stack));
		  		localsMap.put(labels[i], new ArrayList<Object>(locals));
		  }
		  stacks.put(dflt, new ArrayList<Object>(stack));
		  localsMap.put(dflt, new ArrayList<Object>(locals));
		  //stack = locals = null;
		  //visitInsn1(Opcodes.TABLESWITCH);
	  }

	  @Override
	  public void visitLookupSwitchInsn(final Label dflt, final int[] keys, final Label[] labels) {
		  //System.out.println("Lookupswitch Opcode. Stack Size: " + stack.size() + " before exec");
		  if(mv!=null)
			  mv.visitLookupSwitchInsn(dflt, keys, labels);
		  stack.remove(stack.size() - 1);
		  for (int i=0; i< labels.length; i++)
		  {		stacks.put(labels[i], new ArrayList<Object>(stack));
		  		localsMap.put(labels[i], new ArrayList<Object>(locals));
	  	  }
		  stacks.put(dflt, new ArrayList<Object>(stack));
		  localsMap.put(dflt, new ArrayList<Object>(locals));
		  //stack = locals = null;
		  //visitInsn1(Opcodes.LOOKUPSWITCH);
	  }

	  
	  public void printStack()
	  {
		  //System.out.println("\nSize: " + stack.size());
		  for (int i=0; i< stack.size(); i++)
			  System.out.println(stack.get(i));
	  }
}

