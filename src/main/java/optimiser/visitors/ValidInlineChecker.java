package optimiser.visitors;


import java.util.HashMap;
import java.util.HashSet;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.tree.MethodNode;

import optimiser.JarOptimiser;
import optimiser.structs.CallSites;


public class ValidInlineChecker extends MethodVisitor implements CallSiteHelper{

	/* 
	 * The Java Virtual Machine instructions anewarray, checkcast, getfield, getstatic, instanceof, invokedynamic, invokeinterface,
	 * invokespecial, invokestatic, invokevirtual, ldc, ldc_w, multianewarray, new, putfield,
	 * and putstatic make symbolic references to the run-time constant pool. 
	 * Execution of any of these instructions requires resolution of its symbolic reference */
	
	JarOptimiser jarOpt;
	
	boolean inlinable;
	
	int current_depth;
	int max_depth;
	boolean overridesExternal;
	boolean isSameClass;
	boolean nonEmptyCallerStack;
	
	HashMap<String, String[]> toStaticCallSites;
	HashMap<String, String[]> toInlineCallSites;
	protected HashMap<String, Integer> method_callsites;
	
	public ValidInlineChecker(JarOptimiser jarOpt,
							  String calleeClass, String calleeSig,
							  int current_depth, int max_depth, boolean isSameClass, boolean nonEmptyCallerStack) {
		super(Opcodes.ASM7);
		this.jarOpt = jarOpt;
		inlinable = true;
		
		this.current_depth = current_depth;
		this.max_depth = max_depth;
		this.overridesExternal = jarOpt.getExternalOverriders().get(calleeClass);
		this.isSameClass = isSameClass;
		this.nonEmptyCallerStack = nonEmptyCallerStack;
		
		//this.allStaticCallSites= allStaticCallSites;
		this.toStaticCallSites = jarOpt.getSingleInvocs().get(calleeClass, calleeSig);
		//this.allInlineCallSites = allInlineCallSites;
		this.toInlineCallSites = jarOpt.getSingleInvocs_inline().get(calleeClass, calleeSig);
		
		this.method_callsites = new HashMap<String, Integer>();
		
	}

	private void visitInsn(String owner, String name, String descr) {
		
		String full_sig = owner + "\t" + name + "\t" + descr;
		
		if (overridesExternal  && !jarOpt.getPublicMembers().containsKey(full_sig))
				inlinable = false;
		
		/*
		if (overridesExternal  )
			if ( !publicMembers.containsKey(full_sig) || publicMembers.get(full_sig) != Opcodes.ACC_PUBLIC)
				inlinable = false;
		// NOT EXTERNAL OVERRIDE
		else if (publicMembers.containsKey(full_sig) && publicMembers.get(full_sig) == Opcodes.ACC_PRIVATE)
			inlinable = false;*/
		
				
	}

	@Override
	public void visitCode() {
	    
	}
	
	@Override
	public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
		
		visitInsn(owner, name, descriptor);
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {

		if (!inlinable || isSameClass)
			// Callee method is already found to not be inlinable OR
			// Caller and Callee belong in the same class (hence we don't care abount invokespecials)
			// return and save time.
			return;
		
		String callSite = getCallSite( method_callsites, owner, name);
		String full_sig = owner + "\t" + name + "\t" + descriptor;
		
		// TRYING TO GET THE DYNAMIC SIG AND CLASS OF THE METHOD THAT IS ACTUALLY CALLED
		// IF NOT FOUND. USE STATIC
		String[] ownerANDretType = toStaticCallSites.get(callSite);
		String retType = null ;
		String dynOwner  = owner ;
		String dynamic_sig = name + "\t" + descriptor;
		if (toInlineCallSites.containsKey(callSite))
			ownerANDretType = toInlineCallSites.get(callSite);
		if (ownerANDretType != null)
		{	retType = ownerANDretType[1];
			dynOwner = ownerANDretType[0];
			dynamic_sig = name + "\t" + descriptor.split("\\)", 2)[0] + ")" + retType;
			full_sig = dynOwner + "\t" + dynamic_sig;
		}
		
		//OVERRIDES EXTERNAL CLASS AND THERE IS SOMETHING NOT KNOWN TO THE APPLICATION. ABORT.
		HashMap<String, Integer> publicMembers = jarOpt.getPublicMembers();
		if (overridesExternal  && !publicMembers.containsKey(full_sig))
		{	inlinable = false;
			return;
		}
		

		
			
		if (opcode == Opcodes.INVOKESPECIAL &&
				(!name.equals("<init>") || (publicMembers.containsKey(full_sig) && publicMembers.get(full_sig) == Opcodes.ACC_PRIVATE )))
				
		{	
			// THERE ARE STILL INVOCESPECIALS, YET MAXIMUM DEPTH IS REACHED. ABORT INLINE
			if (current_depth >= max_depth)
				inlinable = false;
			// RECURSIVELY CHECK INVOKESPECIALS INSNS OF THE TARGET METHOD OF THIS INVOKESPECIAL
			else if (toInlineCallSites.containsKey(callSite))
			{
				//String[] ownerANDretType = toInlineCallSites.get(callSite);
				//String retType = ownerANDretType[1];
				//String dynOwner = ownerANDretType[0];
				//String dynamic_sig = name + "\t" + descriptor.split("\\)", 2)[0] + ")" + retType;
					
				ValidInlineChecker vic = new ValidInlineChecker(jarOpt,
											dynOwner, dynamic_sig, current_depth+1, max_depth, isSameClass, nonEmptyCallerStack);
					
				MethodNode mn = jarOpt.getMethsToInline().get(dynOwner).get(dynamic_sig);
					
				mn.accept(vic);
					
				if (!vic.isInlinable())
					inlinable = false;
					
					
			}
			// THIS INVOKESPECIAL IS NOT A CANDIDATE FOR INLINE (i.e Not specified by doop). ABORT INLINE
			else {
				inlinable = false;
			}
		}
		// (PACKAGE) PRIVATE METHOD THAT IS NOT INVOKESPCECIAL
		// WE ASSUME THAT EVERY toStaticCallSite is also toInlineCallSite hence we dont have to check for the second. <--- WRONG
		else if (publicMembers.containsKey(full_sig) && publicMembers.get(full_sig) == Opcodes.ACC_PRIVATE && !toStaticCallSites.containsKey(callSite))
		{
				inlinable = false;
		}
		
			
		
	}
	
	
	@Override
	public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
		if (nonEmptyCallerStack)
			inlinable = false;

	}

	
	
	//TODO DYNAMIC? LDC?
	
	

	@Override
	public void visitParameter(String name, int access) {
		
	}

	@Override
	public AnnotationVisitor visitAnnotationDefault() {
		return null;
	}

	@Override
	public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
		
		return null;
	}

	@Override
	public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
		
		return null;
	}

	@Override
	public void visitAnnotableParameterCount(int parameterCount, boolean visible) {
	}

	@Override
	public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible) {
		return null;
	}

	@Override
	public void visitAttribute(Attribute attribute) {
	}

	@Override
	public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack) {

	}

	@Override
	public void visitInsn(int opcode) {
	}

	@Override
	public void visitIntInsn(int opcode, int operand) {
	}

	@Override
	public void visitVarInsn(int opcode, int var) {
	}

	@Override
	public void visitTypeInsn(int opcode, String type) {

	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor) {

	}

	

	@Override
	public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle,
			Object... bootstrapMethodArguments) {
	}

	@Override
	public void visitJumpInsn(int opcode, Label label) {

	}

	@Override
	public void visitLabel(Label label) {
	
	}

	@Override
	public void visitLdcInsn(Object value) {

	}

	@Override
	public void visitIincInsn(int var, int increment) {

	}

	@Override
	public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {

	}

	@Override
	public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {

	}

	@Override
	public void visitMultiANewArrayInsn(String descriptor, int numDimensions) {

	}

	@Override
	public AnnotationVisitor visitInsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
			return null;
	}


	@Override
	public AnnotationVisitor visitTryCatchAnnotation(int typeRef, TypePath typePath, String descriptor,
			boolean visible) {
		return null;
	}

	@Override
	public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end,
			int index) {

	}

	@Override
	public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start, Label[] end,
			int[] index, String descriptor, boolean visible) {
		return null;
	}

	@Override
	public void visitLineNumber(int line, Label start) {

	}

	@Override
	public void visitMaxs(int maxStack, int maxLocals) {

	}

	@Override
	public void visitEnd() {
		
	}

	public boolean isInlinable() {
		return inlinable;
	}
	
	
}
