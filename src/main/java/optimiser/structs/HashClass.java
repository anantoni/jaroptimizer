package optimiser.structs;

import java.util.HashMap;

public abstract class HashClass<Y> extends HashMap<String, Y> {

	public HashClass() {
		super();
		
	}


	protected abstract Object set_method(String inside_class, String return_type, String method_name, String arguments);
	
	public abstract boolean containsKey(String className, String method_sig);
	
	protected abstract Object get(String className, String method_sig);
	
	

}
