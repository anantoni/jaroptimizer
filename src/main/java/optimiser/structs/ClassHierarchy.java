package optimiser.structs;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.JarEntry;

import org.objectweb.asm.ClassReader;

public class ClassHierarchy extends HashMap<String, String[]> {

	protected HashMap<String, Boolean> ExternalOverriders;
	
	public ClassHierarchy() {
		
	}

	public ClassHierarchy(int initialCapacity) {
		super(initialCapacity);
		
	}

	public ClassHierarchy(Map<String,String[]> m) {
		super(m);
		
	}

	public ClassHierarchy(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
		
	}
	
	
	
	public void put(String className, String superName, String[] interfaces)
	{
		String[] supers = new String[interfaces.length + 1];
		System.arraycopy(interfaces, 0, supers, 0, interfaces.length);
		supers[interfaces.length] = superName;
		put(className, supers);
	}
	
	
	public boolean isStrictSubTypeOf(String subtype, String supertype)
	{
		//strict subtype
		if (subtype.equals(supertype))
			return false;		
		return isSubTypeOf( subtype, supertype);
	}
	
	public boolean isSubTypeOf(String subtype, String supertype)
	{
		// if supertype is external then the statement holds. check here, otherwise the following method would return false
		if (!containsKey(supertype))
			return true;
		
		return isSubTypeOf( subtype, supertype, get(subtype));
	}
	
	private boolean isSubTypeOf(String subtype, String supertype, String[] supers)
	{
		if (subtype.equals(supertype))
			return true;
		else if (supers == null)
			return false;
		
		for (int i=supers.length -1; i >= 0 ; i--)
		{
			if ( isSubTypeOf(supers[i], supertype, get(supers[i])) )
				return true;
		}
		
		return false;
	}
	
	public HashMap<String, Boolean> transformToBool ( ) {
		
		if (ExternalOverriders == null)
		{	
			ExternalOverriders = new HashMap<String, Boolean>();
			
			Iterator<Map.Entry<String, String[]>> it = entrySet().iterator();
			
			while (it.hasNext())
			{	Map.Entry<String,String[]> entry = it.next();
				transformToBool (entry.getKey(), entry.getValue());		
			}
		}
		return ExternalOverriders;
	}
	
	public boolean transformToBool (String className, String[] supers) {
					
			if (ExternalOverriders.containsKey(className))
				return ExternalOverriders.get(className);
			
			//System.out.println(className);
			// EXTERNAL TYPE FOUND
			if (!containsKey(className) && !className.equals("java/lang/Object"))
				return true;
			else if (className.equals("java/lang/Object"))
				return false;
			
			//System.out.println(className + " " + supers.length);
			for (int i =0; i< supers.length; i++)
			{
				String superName = supers[i];
				//if ("org/objectweb/asm/tree/analysis/SourceInterpreter".equals(className))
					//System.out.println(className + " --> " + supers[i]);
				if ((ExternalOverriders.containsKey(superName) && ExternalOverriders.get(superName))
						|| transformToBool(superName, get(superName)) )
				{	
					ExternalOverriders.put(className, true);
					return true;
				}	
			}
			ExternalOverriders.put(className, false);
			return false;
			
			
	}
	
	public void printBool()
	{
		if (ExternalOverriders == null)
			return;
		
		Iterator<Map.Entry<String, Boolean>> it = ExternalOverriders.entrySet().iterator();
		
		System.out.println("\n\\*****************\\");	
		
		while (it.hasNext())
		{	Map.Entry<String,Boolean> entry = it.next();
			System.out.println(entry.getKey() + " " + entry.getValue());	
		}
		
		System.out.println("\\*****************\\\n");	
	}

}
