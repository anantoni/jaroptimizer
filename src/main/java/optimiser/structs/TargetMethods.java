package optimiser.structs;

import java.util.HashMap;
import java.util.HashSet;

import optimiser.parsers.FileParser;

public class TargetMethods extends HashClass<HashSet<String>>
{
		HashSet<String> emptySet;
		
		public TargetMethods()
		{
			super();
			emptySet = new HashSet<String>(0);
		}
	
		@Override
		public Object set_method(String inside_class, String return_type, String method_name, String arguments)
		{
			inside_class = inside_class.replaceAll("\\.","/");
			if (!containsKey(inside_class))
			{
				put(inside_class, new HashSet<String>());
			}
			
			HashSet<String> set = get(inside_class);
			
			//NOTE: FileParser.new_descr_matcher(); must have been called first
			String sig = method_name + "\t" + FileParser.create_descriptor(arguments, return_type);
			if (!set.contains(sig))
				set.add(sig);
			
			return null;
			
		}


		@Override
		public boolean containsKey(String className, String method_sig) {
			
			if (!this.containsKey(className))
				return false;
			
			if (this.get(className).contains(method_sig))
				return true;
			
			return false;
		}
		
		@Override
		protected Object get(String className, String method_sig) {
			
			return null;
			
		}
		
		/*
		@Override 
		public HashSet<String> get(String className)
		{
			//return null;
		}*/
		

}