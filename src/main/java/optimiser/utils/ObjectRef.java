package optimiser.utils;

public class ObjectRef {
	  
	  int id;
	  String staticRef;
	  
	  public ObjectRef(int id, String staticRef) {
		  
		  this.id = id;
		  this.staticRef = staticRef;
	  }
	  
	  @Override
	  public int hashCode()
	  {
		  return id;
	  }
	  
	  @Override
	  public boolean equals(Object obj)
	  {
		  if (!(obj instanceof ObjectRef))
			  return false;
		  
		  if (((ObjectRef)obj).getId() == this.id)
			  return true;
		  return false;
	  }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStaticRef() {
		return staticRef;
	}

	public void setStaticRef(String staticRef) {
		this.staticRef = staticRef;
	}
	  
	  
}
