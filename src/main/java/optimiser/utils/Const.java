package optimiser.utils;

public class Const {
	
	public static final String DEVIRTUAL = "devirtual";
	public static final String INLINE = "inline";
	
	public static final int PUBLIC = 0;
	public static final int PROTECTED = 1;
	public static final int PACKAGE = 2;
	public static final int PRIVATE = 3;

}
