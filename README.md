Introduction
---------
JarOptimizer is a tool that uses as input static (points-to) analysis in order to perform certain optimizations to the bytecode of a java project such as inline, devirtualization of method calls and removal of dynamically unused methods. All of the project bytecode needs to be in a jar file (Uber/Fat jar). The analysis is taken from [Doop](https://bitbucket.org/yanniss/doop/src/master/) - a declarative framework for static analysis of Java programs, centered on pointer analysis algorithms.


Install
---------
./gradlew fatJar

Run
----------
./build/libs/jaroptimizer-all.jar

Example
----------

1) SIMPLISTIC

java -jar build/libs/jaroptimizer-all.jar -jar ./Example/Example2/ex.jar -o ../out2.jar -d 2 -s ./Example/Example2/virtualToStatic.csv -p inline -i ./Example/Example2/inline.csv

THere are two classes inside ./Example/Simplistic/RemoveMethod (A and B).

Method Remove: 
One works fine because the method removed is not used.
The other throws no method found exceptio because the method removed is used.

VirtualToStatic:
Wrapper static functions are added to class A and replace some virtual calls.
Class A works as before

Inline:
Inline only methods that are safe. Use ./Example/Simplistic/Example2 folder

2) PROGUARD

java -cp build/libs/jaroptimizer-all.jar:./Example/proguard.jar optimiser.JarOptimiser -jar ./Example/proguard.jar -o ../out.jar -s ./analysis/context_insensitive/proguard/SingleInvocationTarget.csv

Note that the input jar is also added to the classpath because the ASM library might use Reflection on it.

